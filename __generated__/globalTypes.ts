/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * status
 */
export enum Status {
  ACTIVE = "ACTIVE",
  DRAFT = "DRAFT",
  INACTIVE = "INACTIVE",
}

export enum UserRole {
  ADMIN_VERIFIER = "ADMIN_VERIFIER",
  INDIVIDUAL_VERIFIER = "INDIVIDUAL_VERIFIER",
  SUPER_ADMIN = "SUPER_ADMIN",
  USER = "USER",
}

//==============================================================
// END Enums and Input Objects
//==============================================================
