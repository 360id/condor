export default (file, fileName) => {
    const fileUri = `data:text/${fileName.split('.').pop()};charset=utf-8, ${encodeURIComponent(file)}`;
    const downloadNode = document.createElement('a');
    downloadNode.setAttribute('href', fileUri);
    downloadNode.setAttribute('download', fileName);
    document.body.appendChild(downloadNode);
    downloadNode.click();
    downloadNode.remove();
};
