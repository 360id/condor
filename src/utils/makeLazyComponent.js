import { LoadingScreen } from '../components';
import React, { Suspense, lazy } from 'react';

// usage: makeLazyComponent(() => import('path/to/comp'))
export default lazyImportFn => {
    const Lazy = lazy(lazyImportFn);
    return props => (
        <Suspense fallback={<LoadingScreen isFullHeight noSpacing />}>
            <Lazy {...props} />
        </Suspense>
    );
};
