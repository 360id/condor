export default {
    set: (key: string, data: string): void => localStorage.setItem(key, data),
    remove: (key: string): void => localStorage.removeItem(key),
    get: (key: string): string | null => localStorage.getItem(key),
};
