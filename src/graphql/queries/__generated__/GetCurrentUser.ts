/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetCurrentUser
// ====================================================

export interface GetCurrentUser_getCurrentUser {
  __typename: "UserDto";
  id: string;
  email: string;
  role: string;
  firstName: string | null;
  lastName: string | null;
  organizationId: string | null;
}

export interface GetCurrentUser {
  getCurrentUser: GetCurrentUser_getCurrentUser | null;
}
