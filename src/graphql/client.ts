import { ApolloClient, InMemoryCache, HttpLink, NormalizedCacheObject } from '@apollo/client';

// const getAuthHeaders = () => {
//     const authToken = localStorage.getItem('authToken');
//     if (!authToken) return null;

//     return {
//         authorization: `Bearer ${authToken}`,
//     };
// };

const createApolloClient = (): ApolloClient<NormalizedCacheObject> => {
    const link = new HttpLink({
        uri: 'http://localhost:8000/graphql',
        // headers: getAuthHeaders(),
        credentials: 'include',
    });

    return new ApolloClient({
        link,
        cache: new InMemoryCache(),
        connectToDevTools: true,
    });
};

export { createApolloClient };
