import { gql } from '@apollo/client';

export const REGISTER = gql`
    mutation Register($email: String!, $password: String!, $firstName: String!, $lastName: String!) {
        register(input: { email: $email, password: $password, firstName: $firstName, lastName: $lastName }) {
            user {
                id
                email
                role
                firstName
                lastName
                organizationId
            }
        }
    }
`;

export const REGISTER_ADMIN = gql`
    mutation RegisterAdmin($password: String!, $firstName: String!, $lastName: String!, $token: String!) {
        registerAdmin(input: { password: $password, firstName: $firstName, lastName: $lastName, token: $token }) {
            ok
        }
    }
`;
