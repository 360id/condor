/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: RegisterAdmin
// ====================================================

export interface RegisterAdmin_registerAdmin {
  __typename: "SuccessFailureResponse";
  ok: boolean;
}

export interface RegisterAdmin {
  registerAdmin: RegisterAdmin_registerAdmin;
}

export interface RegisterAdminVariables {
  password: string;
  firstName: string;
  lastName: string;
  token: string;
}
