import React, { useState, useContext, createContext } from 'react';
import { ApolloProvider, ApolloClient, InMemoryCache, gql, NormalizedCacheObject } from '@apollo/client';
import { createApolloClient } from './graphql/client';

type Login = {
    email: string;
    password: string;
};

type User = {
    email?: string;
    role?: string;
};

interface IContextValue {
    setAuthToken: React.Dispatch<React.SetStateAction<null>>;
    isSignedIn: () => boolean;
    // signIn: ({}: Login) => Promise<void>;
    signOut: () => void;
    createApolloClient: () => ApolloClient<NormalizedCacheObject>;
    // getUser: () => Promise<ApolloQueryResult<User> | undefined>;
}

const authContext = createContext<IContextValue>({
    setAuthToken: () => undefined,
    isSignedIn: () => false,
    // signIn: () => new Promise(resolve => resolve),
    signOut: () => undefined,
    createApolloClient: () => new ApolloClient({ uri: '', cache: new InMemoryCache() }),
    // getUser: () => new Promise(resolve => resolve),
});

export const AuthProvider: React.FC = ({ children }) => {
    const auth = useProvideAuth();

    return (
        <authContext.Provider value={auth}>
            <ApolloProvider client={createApolloClient()}>{children}</ApolloProvider>
        </authContext.Provider>
    );
};

export const useAuth = (): IContextValue => {
    return useContext(authContext);
};

function useProvideAuth() {
    const [authToken, setAuthToken] = useState(null);

    const isSignedIn = () => {
        const user = localStorage.getItem('authToken');
        return user ? true : false;
    };

    // const signIn = async ({ email, password }: Login) => {
    //     const client = createApolloClient();
    //     const LoginMutation = gql`
    //         mutation Login($email: String!, $password: String!) {
    //             login(input: { email: $email, password: $password }) {
    //                 token
    //                 user {
    //                     email
    //                     role
    //                 }
    //             }
    //         }
    //     `;

    //     const result = await client.mutate({
    //         mutation: LoginMutation,
    //         variables: { email, password },
    //     });

    //     console.log('Result', result);

    //     if (result?.data?.login?.token) {
    //         setAuthToken(result.data.login.token);
    //         localStorage.setItem('authToken', result.data.login.token);
    //     }
    // };

    const signOut = async () => {
        const client = createApolloClient();
        const LogoutMutation = gql`
            mutation Logout {
                logout {
                    ok
                }
            }
        `;

        const logoutResult = await client.mutate({ mutation: LogoutMutation });
        if (logoutResult.data?.logout.ok) {
            setAuthToken(null);
            localStorage.removeItem('authToken');
            return true;
        }
    };

    // const getUser = async () => {
    //     // if (authChecked) return;

    //     // if (!localStorage.getItem('authToken')) {
    //     //     setAuthChecked(true);
    //     //     return;
    //     // }

    //     try {
    //         const client = createApolloClient();
    //         const AuthenticateQuery = gql`
    //             query GetCurrentUser {
    //                 getCurrentUser {
    //                     email
    //                     role
    //                 }
    //             }
    //         `;
    //         const res = await client.query({ query: AuthenticateQuery });
    //         console.log('Res', res);
    //         return res;
    //     } catch (e) {
    //         console.error(e);
    //         signOut();
    //     }
    // };

    return {
        setAuthToken,
        isSignedIn,
        // signIn,
        signOut,
        createApolloClient,
        // getUser,
    };
}
