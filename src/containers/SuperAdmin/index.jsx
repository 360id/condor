import { AnimatedSwitch, NavigationBar, ErrorBoundary, MainTopBar } from '../../components';
import { AppBar, Container, Box } from '@material-ui/core';
import SingleOrganization from './SingleOrganization';
import { makeStyles } from '@material-ui/core/styles';
import { useLocation } from 'react-router-dom';
import Organizations from './Organizations';
import Notifications from './Notifications';
import { useSelector } from 'react-redux';
import Dashboard from './Dashboard';
import Customers from './Customers';
import { get } from 'lodash';
import React from 'react';

const useStyles = makeStyles(() => ({
    currPageData: {
        padding: '18px 24px',
        fontSize: '16px',
    },
}));

const routes = [
    { exact: true, path: '/', title: 'Dashboard', component: Dashboard },
    { exact: true, path: '/organizations', title: 'Organizations', component: Organizations },
    { exact: true, path: '/customers', title: 'Customers', component: Customers },
    { exact: true, path: '/notifications', title: 'Notifications', component: Notifications },
    { exact: true, path: '/organizations/:organizationId', component: SingleOrganization },
];

export default React.memo(() => {
    // const store = useSelector(state => state.organization);
    const store = '';
    const location = useLocation();
    const classes = useStyles();

    const renderHeaderData = () => {
        const curr = location.pathname.split('/')[1];
        const pages = {
            organizations: () => {
                const currOrg = get(store, 'organization.businessName', '');
                return (
                    <span>
                        <span style={{ fontWeight: 600 }}>Organizations</span>
                        {currOrg ? <span style={{ padding: '0px 15px' }}>|</span> : ''}
                        {currOrg}
                    </span>
                );
            },
        };
        return <div className={classes.currPageData}>{pages[curr] ? pages[curr]() : null}</div>;
    };

    const isNotRootRoute = location.pathname.split('/').length > 2;

    return (
        <Box>
            <AppBar position='static'>
                <MainTopBar isBackIcon={isNotRootRoute} headerData={isNotRootRoute && renderHeaderData()} />
                {!isNotRootRoute && <NavigationBar routes={routes} />}
            </AppBar>
            <Box mt={3}>
                <Container maxWidth={false}>
                    <ErrorBoundary>
                        <AnimatedSwitch routes={routes} isNotFoundRoute />
                    </ErrorBoundary>
                </Container>
            </Box>
        </Box>
    );
});
