import { Table, TableHeader, MainLayout, LoadingScreen } from '../../../components';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import AddEditCustomerModal from './AddEditCustomerModal';
// import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
// import customerSlice from '../../../slices/customer';
import { Paper, Grid } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useQuery, gql, useMutation } from '@apollo/client';

const GET_CUSTOMERS = gql`
    query GetCustomers {
        getCustomers {
            id
            firstName
            lastName
            email
            phoneNumber
        }
    }
`;

const DELETE_CUSTOMER = gql`
    mutation DeleteCustomer($id: String!) {
        deleteCustomer(id: $id) {
            ok
        }
    }
`;

const useStyles = makeStyles(() => ({
    iconWrap: {
        display: 'flex',
        justifyContent: 'center',
    },
    iconDriver: {
        fontSize: '28px',
        color: '#3895f1',
    },
    iconFacial: {
        fontSize: '32px',
        color: '#3895f1',
    },
}));

export default React.memo(() => {
    // const { customersLoading, customersList } = useSelector(state => state.customer);
    const [isAddEditModal, setIsAddEditModal] = useState(false);
    const [rowToEdit, setRowToEdit] = useState(null);
    // const dispatch = useDispatch();
    const classes = useStyles();

    const { loading, error, data } = useQuery(GET_CUSTOMERS);
    const [deleteCustomer] = useMutation(DELETE_CUSTOMER);
    console.log('CustomerData', data);

    // useEffect(() => {
    //     dispatch(customerSlice.thunks.getCustomersList());
    // }, [dispatch]);

    const columns = React.useMemo(
        () => [
            {
                Header: 'First Name',
                accessor: 'firstName',
            },
            {
                Header: 'Last Name',
                accessor: 'lastName',
            },
            {
                Header: `Driver's Lisence`,
                accessor: 'driverLicence',
                Cell: () => (
                    <div className={classes.iconWrap}>
                        <AccountBoxIcon className={classes.iconDriver} />
                    </div>
                ),
            },
            {
                Header: 'Facial Record',
                accessor: 'facialRecords',
                Cell: () => (
                    <div className={classes.iconWrap}>
                        <RecentActorsIcon className={classes.iconFacial} />
                    </div>
                ),
            },
            {
                Header: 'Email',
                accessor: 'email',
            },
            {
                Header: 'Phone',
                accessor: 'phoneNumber',
                Cell: ({ value }) => value || '-',
            },
        ],
        [classes.iconWrap, classes.iconDriver, classes.iconFacial],
    );

    const onSearch = searchText => {
        // dispatch(customerSlice.thunks.getCustomersList({ searchText }));
    };

    const onAddEdit = row => {
        setRowToEdit(row);
        setIsAddEditModal(!isAddEditModal);
    };

    const actions = {
        Edit: row => onAddEdit(row.original),
        Delete: row => ({
            confirm: `Are you sure you want to delete ${row.original.firstName || ''} ${row.original.lastName || ''}?`,
            removeRow: () => deleteCustomer({ variables: { id: row.original.id } }),
            // func: () => dispatch(customerSlice.thunks.deleteCustomer(row.original.id)),
        }),
    };

    if (loading || !data) return <LoadingScreen />;
    const customersList = data.getCustomers;

    return (
        <MainLayout isDefaultPadding>
            {isAddEditModal && <AddEditCustomerModal onCancel={() => onAddEdit()} rowToEdit={rowToEdit} />}
            <Grid item xs={12}>
                <TableHeader
                    title='Customers'
                    onSearch={onSearch}
                    onAdd={() => onAddEdit()}
                    onAddLabel='Add Customer'
                />
                <Paper>
                    <Table loading={loading} data={customersList} columns={columns} actions={actions} />
                </Paper>
            </Grid>
        </MainLayout>
    );
});
