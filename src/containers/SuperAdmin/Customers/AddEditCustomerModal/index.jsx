import { Container, Button, Grid, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
// import customerSlice from '../../../../slices/customer';
import { makeStyles } from '@material-ui/core/styles';
import { Modal } from '../../../../components';
import { TextField } from 'formik-material-ui';
import { Formik, Field, Form } from 'formik';
import { USER } from '../../../../constants';
// import { useDispatch } from 'react-redux';
import React, { useRef } from 'react';
import { pick } from 'lodash';
import * as Yup from 'yup';
import { gql, useMutation } from '@apollo/client';

const CREATE_CUSTOMER = gql`
    mutation CreateCustomer($email: String!) {
        createCustomer(input: { email: $email }) {
            customer {
                id
                email
            }
        }
    }
`;

const UPDATE_CUSTOMER = gql`
    mutation UpdateCustomer($id: String!, $email: String, $firstName: String, $lastName: String) {
        updateCustomer(id: $id, input: { email: $email, firstName: $firstName, lastName: $lastName }) {
            ok
        }
    }
`;

const useStyles = makeStyles(theme => ({
    container: {
        width: '630px',
    },
    addButtonsWrap: {
        marginLeft: '16px',
    },
    addButton: {
        textTransform: 'none',
        marginLeft: '16px',
        fontSize: '14px',
    },
}));

const validationSchema = {
    email: Yup.string().email('Invalid Email').required('Required!'),
};

export default React.memo(({ onCancel, rowToEdit }) => {
    // const dispatch = useDispatch();
    const classes = useStyles();
    const myFormRef = useRef();

    const [createCustomer] = useMutation(CREATE_CUSTOMER);
    const [updateCustomer] = useMutation(UPDATE_CUSTOMER);

    const renderButtons = () => (
        <div className={classes.addButtonsWrap}>
            <Button
                onClick={() => myFormRef.current.handleSubmit()}
                className={classes.addButton}
                variant='contained'
                color='primary'>
                Save
            </Button>
            <Button className={classes.addButton} variant='outlined' onClick={onCancel}>
                Cancel
            </Button>
        </div>
    );

    const onSubmit = async values => {
        const body = {
            ...pick(values, ['email']),
            data: pick(values, ['firstName', 'lastName', 'phoneNumber', 'mobileProvider']),
            ...(rowToEdit ? { id: rowToEdit.id } : {}),
        };

        const result = body.id
            ? await updateCustomer({
                  variables: { id: body.id, email: body.email, firstName: body.firstName, lastName: body.lastName },
              })
            : await createCustomer({ variables: { email: body.email } });
        if (result) onCancel();
    };

    const getInitialValues = () => {
        if (rowToEdit) {
            return {
                ...pick(rowToEdit, ['email', 'firstName', 'lastName', 'phoneNumber']),
            };
        }
        const values = {};
        Object.keys(validationSchema).forEach(key => {
            values[key] = '';
        });
        return values;
    };

    const defaultProps = {
        component: TextField,
        margin: 'normal',
        fullWidth: true,
        required: true,
    };

    return (
        <Modal
            title={`${rowToEdit ? 'Edit' : 'Add'} Customer`}
            customButtons={renderButtons()}
            onCancel={onCancel}
            buttonsPos='start'
            dividers={false}
            isOpen>
            <Container className={classes.container}>
                <Formik
                    validationSchema={Yup.object().shape(validationSchema)}
                    initialValues={getInitialValues()}
                    innerRef={myFormRef}
                    onSubmit={onSubmit}>
                    {({ values, setFieldValue, errors, touched }) => (
                        <Form noValidate>
                            {rowToEdit ? (
                                <>
                                    <Field {...defaultProps} label='First Name' name='firstName' />
                                    <Field {...defaultProps} label='Last Name' name='lastName' />
                                </>
                            ) : null}
                            <Field {...defaultProps} label='Email' name='email' autoComplete='email' type='email' />
                        </Form>
                    )}
                </Formik>
            </Container>
        </Modal>
    );
});
