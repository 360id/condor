/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateCustomer
// ====================================================

export interface CreateCustomer_createCustomer_customer {
  __typename: "UserDto";
  id: string;
  email: string;
}

export interface CreateCustomer_createCustomer {
  __typename: "GenericResponse";
  customer: CreateCustomer_createCustomer_customer | null;
}

export interface CreateCustomer {
  createCustomer: CreateCustomer_createCustomer;
}

export interface CreateCustomerVariables {
  email: string;
}
