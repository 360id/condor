/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateCustomer
// ====================================================

export interface UpdateCustomer_updateCustomer {
  __typename: "SuccessFailureResponse";
  ok: boolean;
}

export interface UpdateCustomer {
  updateCustomer: UpdateCustomer_updateCustomer;
}

export interface UpdateCustomerVariables {
  id: string;
  email?: string | null;
  firstName?: string | null;
  lastName?: string | null;
}
