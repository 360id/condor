/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetCustomers
// ====================================================

export interface GetCustomers_getCustomers {
  __typename: "UserDto";
  id: string;
  firstName: string | null;
  lastName: string | null;
  email: string;
  phoneNumber: string | null;
}

export interface GetCustomers {
  getCustomers: GetCustomers_getCustomers[];
}
