/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteCustomer
// ====================================================

export interface DeleteCustomer_deleteCustomer {
  __typename: "SuccessFailureResponse";
  ok: boolean;
}

export interface DeleteCustomer {
  deleteCustomer: DeleteCustomer_deleteCustomer;
}

export interface DeleteCustomerVariables {
  id: string;
}
