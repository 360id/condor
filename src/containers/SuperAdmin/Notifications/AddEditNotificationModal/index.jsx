import {
    Typography,
    Container,
    Grid,
    Button,
    Select,
    MenuItem,
    Radio,
    RadioGroup,
    FormControlLabel,
    FormControl,
    InputLabel,
    FormHelperText,
} from '@material-ui/core';
import { MuiPickersUtilsProvider, KeyboardTimePicker, KeyboardDatePicker } from '@material-ui/pickers';
// import notificationSlice from '../../../../slices/notification';
import { makeStyles } from '@material-ui/core/styles';
import { NOTIFICATION } from '../../../../constants';
import React, { useRef, useMemo } from 'react';
import { Modal } from '../../../../components';
import { TextField } from 'formik-material-ui';
import { Formik, Field, Form } from 'formik';
// import { useDispatch } from 'react-redux';
import DayJsUtils from '@date-io/dayjs';
import { pick } from 'lodash';
import * as Yup from 'yup';
import dayjs from 'dayjs';

const useStyles = makeStyles(theme => ({
    typography: {
        marginTop: '30px',
        color: '#b4b4b4',
        fontSize: '14px',
        fontWeight: 600,
    },
    messageInput: {
        height: '115px !important',
    },
    addButtonsWrap: {
        marginLeft: '16px',
    },
    addButton: {
        textTransform: 'none',
        marginLeft: '16px',
        fontSize: '14px',
    },
    isEndDate: {
        marginTop: '20px',
    },
    untilDate: {
        marginTop: '0px',
    },
    audienceWrap: {
        marginTop: '20px',
        marginBottom: '10px',
    },
}));

const defaultValues = { messageText: '', hasUntilDate: false, audienceType: '', requestsFrequency: '' };

const validationSchema = {
    messageText: Yup.string().required('Required'),
    startDate: Yup.date().required('Required'),
    hasUntilDate: Yup.boolean().required('Required'),
    untilDate: Yup.date()
        .notRequired()
        .when('hasUntilDate', { is: val => val, then: Yup.date().required('Required') }),
    audienceType: Yup.string().required('Required'),
    requestsFrequency: Yup.string().required('Required'),
};

export default React.memo(({ onCancel, rowToEdit }) => {
    // const dispatch = useDispatch();
    const classes = useStyles();
    const myFormRef = useRef();

    const renderButtons = () => (
        <div className={classes.addButtonsWrap}>
            <Button
                onClick={() => myFormRef.current.handleSubmit()}
                className={classes.addButton}
                variant='contained'
                color='primary'>
                Save push notification
            </Button>
            <Button className={classes.addButton} variant='outlined' onClick={onCancel}>
                Cancel
            </Button>
        </div>
    );

    const onSubmit = async values => {
        const startDate = dayjs(values.startDate).format();
        const untilDate = dayjs(values.untilDate).format();
        const body = { ...values, startDate, untilDate };
        // const res = await dispatch(notificationSlice.thunks.addUpdateNotification(body));
        if (res) onCancel();
    };

    const initialValues = useMemo(() => {
        const startDate = (rowToEdit || {}).startDate ? dayjs(rowToEdit.startDate) : dayjs();
        const untilDate = (rowToEdit || {}).untilDate ? dayjs(rowToEdit.untilDate) : dayjs();
        return {
            ...(rowToEdit ? pick(rowToEdit, [...Object.keys(defaultValues), 'id']) : defaultValues),
            startDate,
            untilDate,
        };
    }, [rowToEdit]);

    return (
        <Modal
            title={`${rowToEdit ? 'Edit' : 'Add'} Push Notification`}
            customButtons={renderButtons()}
            onCancel={onCancel}
            buttonsPos='start'
            dividers={false}
            maxWidth='xs'
            fullWidth
            isOpen>
            <Container className={classes.container}>
                <Formik
                    validationSchema={Yup.object().shape(validationSchema)}
                    initialValues={initialValues}
                    innerRef={myFormRef}
                    onSubmit={onSubmit}>
                    {({ values, setFieldValue, errors, touched }) => (
                        <Form noValidate>
                            <Field
                                InputProps={{ classes: { input: classes.messageInput }, multiline: true }}
                                component={TextField}
                                name='messageText'
                                variant='outlined'
                                label='Message'
                                fullWidth
                                required
                            />
                            <Typography className={classes.typography}>Schedule</Typography>
                            <MuiPickersUtilsProvider utils={DayJsUtils}>
                                <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <KeyboardDatePicker
                                            KeyboardButtonProps={{ 'aria-label': 'change date' }}
                                            onChange={date => setFieldValue('startDate', date)}
                                            value={values.startDate}
                                            format='MM/DD/YYYY'
                                            name='startDate'
                                            margin='normal'
                                            label='Date'
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <KeyboardTimePicker
                                            onChange={date => setFieldValue('startDate', date)}
                                            KeyboardButtonProps={{ 'aria-label': 'change time' }}
                                            value={values.startDate}
                                            name='startDate'
                                            margin='normal'
                                            label='Time'
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormControl
                                            error={!!errors.requestsFrequency && touched.requestsFrequency}
                                            fullWidth
                                            required>
                                            <InputLabel>Repeats</InputLabel>
                                            <Select
                                                onChange={e => setFieldValue('requestsFrequency', e.target.value)}
                                                value={values.requestsFrequency || ''}
                                                name='requestsFrequency'>
                                                {NOTIFICATION.REPEATS_LIST.map(({ label, value }) => (
                                                    <MenuItem key={value} value={value}>
                                                        {label}
                                                    </MenuItem>
                                                ))}
                                            </Select>
                                            {touched.requestsFrequency && (
                                                <FormHelperText>{errors.requestsFrequency}</FormHelperText>
                                            )}
                                        </FormControl>
                                        <RadioGroup
                                            onChange={e => setFieldValue('hasUntilDate', e.target.value === 'true')}
                                            className={classes.isEndDate}
                                            value={!!values.hasUntilDate}
                                            name='hasUntilDate'
                                            row>
                                            <FormControlLabel
                                                control={<Radio color='primary' />}
                                                checked={!values.hasUntilDate}
                                                label='No end date'
                                                value='false'
                                            />
                                            <FormControlLabel
                                                control={<Radio color='primary' />}
                                                checked={!!values.hasUntilDate}
                                                label='Until'
                                                value='true'
                                            />
                                        </RadioGroup>
                                        {values.hasUntilDate && (
                                            <KeyboardDatePicker
                                                onChange={date => setFieldValue('untilDate', date)}
                                                KeyboardButtonProps={{ 'aria-label': 'change date' }}
                                                className={classes.untilDate}
                                                value={values.untilDate}
                                                format='MM/DD/YYYY'
                                                name='untilDate'
                                                margin='normal'
                                                fullWidth
                                            />
                                        )}
                                        <FormControl
                                            error={!!errors.audienceType && touched.audienceType}
                                            className={classes.audienceWrap}
                                            fullWidth
                                            required>
                                            <InputLabel>Audience</InputLabel>
                                            <Select
                                                onChange={e => setFieldValue('audienceType', e.target.value)}
                                                value={values.audienceType || ''}
                                                name='audienceType'>
                                                {NOTIFICATION.AUDIENCES_LIST.map(({ label, value }) => (
                                                    <MenuItem key={value} value={value}>
                                                        {label}
                                                    </MenuItem>
                                                ))}
                                            </Select>
                                            {touched.audienceType && (
                                                <FormHelperText>{errors.audienceType}</FormHelperText>
                                            )}
                                        </FormControl>
                                    </Grid>
                                </Grid>
                            </MuiPickersUtilsProvider>
                        </Form>
                    )}
                </Formik>
            </Container>
        </Modal>
    );
});
