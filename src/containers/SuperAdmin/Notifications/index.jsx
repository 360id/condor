import { Table, TableHeader, MainLayout } from '../../../components';
import AddEditNotificationModal from './AddEditNotificationModal';
// import notificationSlice from '../../../slices/notification';
import { Paper, Grid, Switch } from '@material-ui/core';
// import { useDispatch, useSelector } from 'react-redux';
import React, { useEffect, useState } from 'react';
import { NOTIFICATION } from '../../../constants';
import dayjs from 'dayjs';

export default React.memo(() => {
    // const { notificationsLoading, notificationsList } = useSelector(state => state.notification);
    const [isAddEditModal, $isAddEditModal] = useState(false);
    const [rowToEdit, $rowToEdit] = useState(null);
    const notificationsLoading = false;
    const notificationsList = [];
    // const dispatch = useDispatch();

    // useEffect(() => {
    //     dispatch(notificationSlice.thunks.getNotificationsList());
    // }, [dispatch]);

    const prepareStatus = status => {
        const mess = +dayjs(status) < +dayjs() ? 'Last' : 'To be';
        return `${mess} sent: ${dayjs(status).format('MMMM DD, YYYY')}`;
    };

    const columns = React.useMemo(
        () => [
            {
                Header: 'Message',
                accessor: 'messageText',
            },
            {
                Header: 'Status',
                accessor: 'status',
                Cell: ({ row }) => (row.original.lastSendDate ? prepareStatus(row.original.lastSendDate) : 'Draft'),
            },
            {
                Header: 'Type',
                accessor: 'requestsFrequency',
                Cell: ({ value }) => NOTIFICATION.REPEATS_LABELS[value] || value,
            },
            {
                Header: 'Set up',
                accessor: 'isActive',
                Cell: ({ row }) => (
                    <Switch
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                        checked={row.original.isActive}
                        color='primary'
                    />
                ),
            },
        ],
        [],
    );

    // onChange={e => handleIsActive(row.original.id, e.target.checked)}
    // const handleIsActive = async (id, isActive) => {
    //   console.log({ id, isActive })
    //   await dispatch(notificationSlice.thunks.addUpdateNotification({ id, isActive }))
    // }

    const onSearch = searchText => {
        // dispatch(notificationSlice.thunks.getNotificationsList({ searchText }));
    };

    const onAddEdit = row => {
        $rowToEdit(row);
        $isAddEditModal(!isAddEditModal);
    };

    const actions = {
        Edit: row => onAddEdit(row.original),
        Delete: row => ({
            confirm: 'Are you sure you want to delete this notification',
            // func: () => dispatch(notificationSlice.thunks.deleteNotification(row.original.id)),
        }),
    };

    return (
        <MainLayout isDefaultPadding>
            {isAddEditModal && <AddEditNotificationModal onCancel={() => onAddEdit()} rowToEdit={rowToEdit} />}
            <Grid item xs={12}>
                <TableHeader
                    title='Notifications'
                    onSearch={onSearch}
                    onAdd={() => onAddEdit()}
                    onAddLabel='Add Notification'
                />
                <Paper>
                    <Table
                        loading={notificationsLoading}
                        data={notificationsList}
                        actions={actions}
                        columns={columns}
                    />
                </Paper>
            </Grid>
        </MainLayout>
    );
});
