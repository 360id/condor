import { Grid, Card, CardContent, Typography, Divider } from '@material-ui/core';
import { LoadingCircle, MainLayout, VerifiersList } from '../../../components';
import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import { useParams } from 'react-router';
import { get } from 'lodash';
import { gql, useQuery } from '@apollo/client';

const GET_ORGANIZATION = gql`
    query GetOrganization($id: String!) {
        getOrganization(id: $id) {
            id
            businessName
        }
    }
`;

const useStyles = makeStyles({
    cardRoot: {
        backgroundColor: '#ebebeb',
        padding: '0px 15px',
    },
    cardName: {
        fontSize: '18px',
        fontWeight: 700,
        paddingBottom: '5px',
    },
    cardAddress: {
        fontSize: '14px',
        paddingBottom: '5px',
    },
    cardSubscr: {
        fontSize: '12px',
        textTransform: 'uppercase',
        paddingBottom: '5px',
        fontWeight: 600,
        color: '#3e50b4',
    },
    orgInfoContainer: {
        display: 'flex',
    },
    orgInfoWrap: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '0px 15px',
    },
    orgInfoTitle: {
        textTransform: 'uppercase',
        paddingBottom: '5px',
        fontSize: '12px',
    },
    orgInfoData: {
        textTransform: 'uppercase',
        fontSize: '30px',
    },
});

export default React.memo(() => {
    // const { verifierData, organization, singleOrganizationLoading } = useSelector(state => state.organization);
    const { verifierData } = {};
    const { organizationId } = useParams();
    const totalMembers = 2;
    const adminVerifierCount = 2;
    const orgUsers = [];
    const subscription = 'sub';
    const { loading, data } = useQuery(GET_ORGANIZATION, { variables: { id: organizationId } });
    const organization = data?.getOrganization;
    console.log('Org', organization, loading);

    // const dispatch = useDispatch();
    const classes = useStyles();

    // useEffect(() => {
    //     // dispatch(organizationSlice.thunks.getOrganizationById(organizationId));
    // }, [dispatch, organizationId]);

    const orgInfo = React.useMemo(
        () => [
            {
                title: 'Total members',
                data: totalMembers,
            },
            {
                title: 'Administrators',
                data: adminVerifierCount,
            },
            {
                title: 'Active verifiers',
                data: (orgUsers || []).filter(itm => itm.status === 'active').length,
            },
            {
                title: 'Inactive verifiers',
                data: (orgUsers || []).filter(itm => itm.status === 'inactive').length,
            },
        ],
        [verifierData],
    );

    const renderOrgInfoItem = ({ title, data, isDivider, ind }) => (
        <div className={classes.orgInfoContainer} key={`key-${ind}`}>
            <div className={classes.orgInfoWrap}>
                <Typography className={classes.orgInfoTitle}>{title}</Typography>
                <Typography className={classes.orgInfoData}>{data}</Typography>
            </div>
            {isDivider && <Divider orientation='vertical' variant='middle' />}
        </div>
    );

    return (
        <MainLayout isDefaultPadding>
            <Grid item xs={12}>
                {loading ? (
                    <LoadingCircle height='100px' />
                ) : (
                    <Card className={classes.cardRoot}>
                        <CardContent>
                            <Grid container>
                                <Grid item xs={4}>
                                    <Typography className={classes.cardName}>{organization.businessName}</Typography>
                                    <Typography className={classes.cardAddress}>{`Address: Brisban Loac`}</Typography>
                                    <Typography className={classes.cardSubscr}>{`Subscription: ${
                                        subscription || '-'
                                    }`}</Typography>
                                </Grid>
                                <Grid container justify='flex-end' xs={8}>
                                    {orgInfo.map((itm, ind) =>
                                        renderOrgInfoItem({ ...itm, ind, isDivider: orgInfo.length !== ind + 1 }),
                                    )}
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                )}
            </Grid>
            <VerifiersList />
        </MainLayout>
    );
});
