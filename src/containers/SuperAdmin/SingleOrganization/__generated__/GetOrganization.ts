/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetOrganization
// ====================================================

export interface GetOrganization_getOrganization {
  __typename: "Organization";
  id: string;
  businessName: string;
}

export interface GetOrganization {
  getOrganization: GetOrganization_getOrganization;
}

export interface GetOrganizationVariables {
  id: string;
}
