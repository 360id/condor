import { MainLayout, Chart, LoadingCircle } from '../../../components';
// import organizationSlice from '../../../slices/organization';
// import { useDispatch, useSelector } from 'react-redux';
// import customerSlice from '../../../slices/customer';
// import verifierSlice from '../../../slices/verifier';
import { Grid } from '@material-ui/core';
import React, { useEffect } from 'react';

const xLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October'];

export default React.memo(() => {
    // const { customersMetricsLoading, customersMetricsList } = useSelector(state => state.customer);
    // const { verifierMetricsLoading, verifierMetricsList } = useSelector(state => state.verifier);
    // const { usersMetricsLoading, usersMetricsList } = useSelector(state => state.organization);
    const { customersMetricsLoading, customersMetricsList } = {};
    const { verifierMetricsLoading, verifierMetricsList } = {};
    const { usersMetricsLoading, usersMetricsList } = {};
    // const dispatch = useDispatch();

    // useEffect(() => {
    //     dispatch(organizationSlice.thunks.getUsersMetrics());
    //     dispatch(customerSlice.thunks.getCustomerMetrics());
    //     dispatch(verifierSlice.thunks.getVerifierMetrics());
    // }, [dispatch]);

    const charts = [
        {
            datasetProps: { backgroundColor: '#a5c4fc' },
            title: 'Verifiers organization metrics',
            isLoading: verifierMetricsLoading,
            data: verifierMetricsList,
        },
        {
            datasetProps: { backgroundColor: '#8EDFFF' },
            title: 'User scans per organization metrics',
            isLoading: usersMetricsLoading,
            data: usersMetricsList,
            type: 'horizontalBar',
        },
        {
            datasetProps: { borderWidth: 2, fill: false, pointRadius: 2, borderColor: '#34A2EB' },
            title: 'Event logs',
            isLoading: false,
            data: [-45, 98, -78, 15, -100, 15, 19, 23, -90],
            type: 'line',
        },
        {
            datasetProps: { borderWidth: 3, fill: false, lineTension: 0, borderColor: '#279AD2' },
            isLoading: customersMetricsLoading,
            data: customersMetricsList,
            title: 'Customer metrics',
            type: 'line',
        },
    ];

    return (
        <MainLayout isDefaultPadding>
            {charts.map(({ isLoading, data, ...rest }, ind) => (
                <Grid item xs={6} key={`key-${ind}`}>
                    {isLoading ? (
                        <LoadingCircle height='300px' />
                    ) : (
                        <Chart simpleData={data} xLabels={xLabels} {...rest} />
                    )}
                </Grid>
            ))}
        </MainLayout>
    );
});
