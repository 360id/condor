import { Table, TableHeader, MainLayout, Status, LoadingScreen } from '../../../components';
import AddEditOrganizationModal from './AddEditOrganizationModal';
import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect, useState } from 'react';
import { Paper, Grid } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { useQuery, useMutation, gql } from '@apollo/client';
import { Row, CellProps, Column } from 'react-table';
import { ContactSupportOutlined } from '@material-ui/icons';

export const GET_ORGANIZATIONS = gql`
    query GetOrganizations {
        getOrganizations {
            id
            businessName
            masterKey
            users {
                email
            }
            status
        }
    }
`;

const DELETE_ORGANIZATION = gql`
    mutation DeleteOrganization($id: String!) {
        deleteOrganization(id: $id) {
            ok
        }
    }
`;

const useStyles = makeStyles(() => ({
    statusIcon: {
        width: '14px',
        height: '14px',
        position: 'relative',
        top: '3px',
        left: '-3px',
    },
    statusIconActive: {
        fill: '#2dc756',
    },
    statusIconDraft: {
        fill: '#eab75e',
    },
    statusIconInactive: {
        fill: '#b4b4b4',
    },
    singleOrgLink: {
        color: '#2177c1',
        textDecoration: 'none',
        fontWeight: 700,
    },
}));

type OrgData = {
    email: string;
};
type User = {
    email: string;
    firstName: string;
    phoneNumber: string;
};

type Data = {
    businessAddress: 'string';
    email: any;
    firstName: any;
    lastName: any;
    state: any;
    city: any;
    zip: any;
};

type BusinessAddress = {
    addressLine1: string;
    addressLine2?: string;
    state: string;
    city: string;
    zip: string;
};

export type OrganizationData = {
    id: string;
    businessName: string;
    subscription?: string;
    users: User[];
    administrator?: string;
    mobile?: string;
    masterKey?: string;
    email?: string;
    status: string;
    businessAddress?: BusinessAddress;
};

const Organizations: React.FC = () => {
    const { loading, error, data } = useQuery(GET_ORGANIZATIONS);
    const [deleteOrganization] = useMutation(DELETE_ORGANIZATION);

    const [isAddEditModal, $isAddEditModal] = useState(false);
    const [rowToEdit, $rowToEdit] = useState<OrganizationData>();
    const classes = useStyles();

    const columns: Column<OrganizationData>[] = React.useMemo(
        () => [
            {
                Header: 'Name',
                accessor: 'businessName',
                Cell: ({ value, row }: CellProps<OrganizationData>) => (
                    <Link className={classes.singleOrgLink} to={`/organizations/${row.original.id}`}>
                        {value}
                    </Link>
                ),
            },
            {
                Header: 'Subscription',
                accessor: 'subscription',
            },
            {
                Header: 'Members',
                accessor: 'users',
                Cell: ({ value = [] }) => value.length,
            },
            {
                Header: 'Administrator',
                accessor: 'administrator',
            },
            {
                Header: 'Email',
                accessor: 'email',
            },
            {
                Header: 'Phone',
                accessor: 'mobile',
            },
            {
                Header: 'Master Key',
                accessor: 'masterKey',
            },
            {
                Header: 'Status',
                accessor: 'status',
                Cell: ({ value }) => <Status value={value} />,
            },
        ],
        [classes.singleOrgLink],
    );

    const onSearch = (searchText: string) => {
        console.log('SearchText', searchText);
        // dispatch(organizationSlice.thunks.getOrganizationList({ searchText }));
    };

    const onAddEdit = (row: OrganizationData | undefined) => {
        console.log('Editing row');
        $rowToEdit(row);
        $isAddEditModal(!isAddEditModal);
    };

    const actions = {
        Edit: (row: Row<OrganizationData>) => onAddEdit(row.original),
        Delete: (row: Row<OrganizationData>) => ({
            confirm: `Are you sure you want to delete ${row.original.businessName}`,
            removeRow: () =>
                deleteOrganization({
                    variables: { id: row.original.id.toString() },
                    update: (cache, { data: { deleteOrganization } }) => {
                        // cache.identify(deleteOrganization);
                        const existing: any = cache.readQuery({ query: GET_ORGANIZATIONS });
                        const newOrganizations = existing.getOrganizations.filter(
                            ({ id }: any) => row.original.id != id,
                        );
                        cache.writeQuery({ query: GET_ORGANIZATIONS, data: { getOrganizations: newOrganizations } });
                    },
                }),
        }),
    };

    if (loading) return <LoadingScreen />;

    const organizationList = data.getOrganizations.slice(0, 5);
    console.log('OrganizationData', organizationList);

    return (
        <MainLayout isDefaultPadding>
            <>
                {isAddEditModal && <AddEditOrganizationModal onCancel={onAddEdit} rowToEdit={rowToEdit} />}
                <Grid item xs={12}>
                    <TableHeader
                        title='Organizations'
                        onSearch={onSearch}
                        onAdd={onAddEdit}
                        onAddLabel='Add Organization'
                    />
                    <Paper>
                        <Table<OrganizationData>
                            loading={loading}
                            data={organizationList}
                            actions={actions}
                            columns={columns}
                        />
                    </Paper>
                </Grid>
            </>
        </MainLayout>
    );
};

export default Organizations;
