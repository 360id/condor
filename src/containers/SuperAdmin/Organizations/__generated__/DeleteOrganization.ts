/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteOrganization
// ====================================================

export interface DeleteOrganization_deleteOrganization {
  __typename: "SuccessFailureResponse";
  ok: boolean;
}

export interface DeleteOrganization {
  deleteOrganization: DeleteOrganization_deleteOrganization;
}

export interface DeleteOrganizationVariables {
  id: string;
}
