/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateOrganization
// ====================================================

export interface UpdateOrganization_updateOrganization {
  __typename: "SuccessFailureResponse";
  ok: boolean;
  message: string;
}

export interface UpdateOrganization {
  updateOrganization: UpdateOrganization_updateOrganization;
}

export interface UpdateOrganizationVariables {
  id: string;
  businessName?: string | null;
  masterKey?: string | null;
  status?: string | null;
  firstName?: string | null;
  lastName?: string | null;
  phoneNumber?: string | null;
  email?: string | null;
  addressLine1?: string | null;
  addressLine2?: string | null;
  city?: string | null;
  state?: string | null;
  zip?: string | null;
}
