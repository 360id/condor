/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateOrganization
// ====================================================

export interface CreateOrganization_createOrganization_organization {
  __typename: "Organization";
  id: string;
}

export interface CreateOrganization_createOrganization {
  __typename: "OrganizationResponse";
  organization: CreateOrganization_createOrganization_organization | null;
}

export interface CreateOrganization {
  createOrganization: CreateOrganization_createOrganization;
}

export interface CreateOrganizationVariables {
  businessName: string;
  masterKey: string;
  status: string;
  firstName: string;
  lastName: string;
  phoneNumber?: string | null;
  email: string;
  addressLine1: string;
  addressLine2?: string | null;
  city: string;
  state: string;
  zip: string;
}
