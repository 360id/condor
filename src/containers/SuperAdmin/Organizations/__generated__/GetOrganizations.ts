/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Status } from "./../../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: GetOrganizations
// ====================================================

export interface GetOrganizations_getOrganizations_users {
  __typename: "User";
  email: string;
}

export interface GetOrganizations_getOrganizations {
  __typename: "Organization";
  id: string;
  businessName: string;
  masterKey: string;
  users: GetOrganizations_getOrganizations_users[];
  status: Status;
}

export interface GetOrganizations {
  getOrganizations: GetOrganizations_getOrganizations[];
}
