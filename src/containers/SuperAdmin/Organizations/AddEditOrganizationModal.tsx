import { Typography, Container, Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React, { useRef, useState } from 'react';
import { Modal } from '../../../components';
import { TextField } from 'formik-material-ui';
import { Formik, Field, Form } from 'formik';
import { Add } from '@material-ui/icons';
import { pick, omit } from 'lodash';
import clname from 'classnames';
import * as Yup from 'yup';
import { Row } from 'react-table';
import { OrganizationData } from './index';
import { gql, useMutation } from '@apollo/client';
import { isEmpty } from 'lodash';
import { GET_ORGANIZATIONS } from './index';
import toast from 'react-hot-toast';

const CREATE_ORGANIZATION = gql`
    mutation CreateOrganization(
        $businessName: String!
        $masterKey: String!
        $status: String!
        $firstName: String!
        $lastName: String!
        $phoneNumber: String
        $email: String!
        $addressLine1: String!
        $addressLine2: String
        $city: String!
        $state: String!
        $zip: String!
    ) {
        createOrganization(
            input: {
                businessName: $businessName
                masterKey: $masterKey
                status: $status
                owner: { firstName: $firstName, lastName: $lastName, email: $email, phoneNumber: $phoneNumber }
                businessAddress: {
                    addressLine1: $addressLine1
                    addressLine2: $addressLine2
                    city: $city
                    state: $state
                    zip: $zip
                }
            }
        ) {
            organization {
                id
            }
        }
    }
`;

const UPDATE_ORGANIZATION = gql`
    mutation UpdateOrganization(
        $id: String!
        $businessName: String
        $masterKey: String
        $status: String
        $firstName: String
        $lastName: String
        $phoneNumber: String
        $email: String
        $addressLine1: String
        $addressLine2: String
        $city: String
        $state: String
        $zip: String
    ) {
        updateOrganization(
            id: $id
            input: {
                businessName: $businessName
                masterKey: $masterKey
                status: $status
                owner: { firstName: $firstName, lastName: $lastName, email: $email, phoneNumber: $phoneNumber }
                businessAddress: {
                    addressLine1: $addressLine1
                    addressLine2: $addressLine2
                    city: $city
                    state: $state
                    zip: $zip
                }
            }
        ) {
            ok
            message
        }
    }
`;

const useStyles = makeStyles(() => ({
    container: {
        width: '630px',
    },
    flexBottom: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },
    masterKey: {
        background: '#ebebeb',
        borderRadius: '15px',
        padding: '0px 20px 20px 20px',
    },
    masterKeyBtn: {
        background: '#3fe5ef',
        textTransform: 'none',
        fontSize: '14px',
        width: '100%',
        marginTop: '10px',
    },
    typography: {
        marginTop: '10px',
        color: '#b4b4b4',
        fontSize: '14px',
        fontWeight: 600,
    },
    typographyKey: {
        marginBottom: '20px',
    },
    InputProps: {
        fontSize: '14px',
    },
    addButtonsWrap: {
        marginLeft: '16px',
    },
    addButton: {
        textTransform: 'none',
        marginLeft: '16px',
        fontSize: '14px',
    },
    addIcon: {
        color: 'black',
    },
}));

const initialValues = {
    businessName: '',
    addressLine1: '',
    firstName: '',
    masterKey: '',
    lastName: '',
    state: '',
    email: '',
    city: '',
    zip: '',
};

const validation = Yup.object().shape({
    email: Yup.string().email('Invalid Email').required('Required'),
    businessName: Yup.string().required('Required'),
    addressLine1: Yup.string().required('Required'),
    firstName: Yup.string().required('Required'),
    masterKey: Yup.string(),
    lastName: Yup.string().required('Required'),
    state: Yup.string().required('Required'),
    city: Yup.string().required('Required'),
    zip: Yup.string().required('Required'),
});

type AddEditOrganizationModalProps = {
    onCancel: (row?: OrganizationData | undefined) => void;
    rowToEdit: OrganizationData | undefined;
};

const AddEditOrganizationModal: React.FC<AddEditOrganizationModalProps> = ({ onCancel, rowToEdit }) => {
    const [isSendEmail, $isSendEmail] = useState(false);
    const classes = useStyles();
    const myFormRef = useRef<any>(null);
    const keyRef = useRef<HTMLInputElement>(null);
    const [createOrganization] = useMutation(CREATE_ORGANIZATION);
    const [updateOrganization] = useMutation(UPDATE_ORGANIZATION);

    const generateKey = (setFieldValue: any) => {
        setFieldValue('masterKey', Math.random().toString(36).substr(2, 9));
        keyRef?.current?.focus();
    };

    const handleSave = (isSnd: any) => {
        $isSendEmail(isSnd);
        myFormRef?.current?.handleSubmit();
    };

    const renderButtons = () =>
        rowToEdit ? (
            <div className={classes.addButtonsWrap}>
                <Button className={classes.addButton} variant='contained' color='primary' onClick={handleSave}>
                    Save
                </Button>
            </div>
        ) : (
            <div className={classes.addButtonsWrap}>
                <Button
                    className={classes.addButton}
                    variant='contained'
                    onClick={() => handleSave(true)}
                    color='primary'>
                    <Add className={classes.addIcon} />
                    Add organization and send key by email
                </Button>
                <Button className={classes.addButton} variant='outlined' onClick={handleSave}>
                    Save as Draft
                </Button>
            </div>
        );

    const onSubmit = async (values: any) => {
        try {
            console.log('Values', values);
            let body: any = {
                ...pick(values, ['businessName', 'masterKey']),
                status: isSendEmail ? 'inactive' : 'draft',
                owner: pick(values, ['firstName', 'lastName', 'email']),
                businessAddress: pick(values, ['addressLine1', 'addressLine2', 'city', 'state', 'zip']),
                ...(rowToEdit ? { id: rowToEdit.id } : {}),
            };
            if (rowToEdit) {
                body = {
                    ...(rowToEdit ? { id: rowToEdit.id } : {}),
                    ...omit(body, 'masterKey'),
                };
            }

            const result = !body.id
                ? await createOrganization({
                      variables: {
                          businessName: body.businessName,
                          status: body.status,
                          masterKey: body.masterKey,
                          firstName: body.owner.firstName,
                          lastName: body.owner.lastName,
                          email: body.owner.email,
                          addressLine1: body.businessAddress.addressLine1,
                          addressLine2: body.businessAddress.addressLine2,
                          city: body.businessAddress.city,
                          state: body.businessAddress.state,
                          zip: body.businessAddress.zip,
                      },
                      refetchQueries: [{ query: GET_ORGANIZATIONS }],
                  })
                : await updateOrganization({
                      variables: {
                          id: body.id,
                          businessName: body.businessName,
                          status: body.status,
                          masterKey: body.masterKey,
                          firstName: body.owner.firstName,
                          lastName: body.owner.lastName,
                          email: body.owner.email,
                          addressLine1: body.businessAddress.addressLine1,
                          addressLine2: body.businessAddress.addressLine2,
                          city: body.businessAddress.city,
                          state: body.businessAddress.state,
                          zip: body.businessAddress.zip,
                      },
                      refetchQueries: [{ query: GET_ORGANIZATIONS }],
                  });
            if (result) {
                toast.success('completed');
                onCancel();
            }
        } catch (e) {
            console.log(e);
            toast.error('something bad happened');
        }
    };

    const getInitialValues = () => {
        if (!rowToEdit) return initialValues;
        return {
            ...initialValues,
            ...pick(rowToEdit, ['businessName', 'masterKey']),
            ...pick(rowToEdit, ['firstName', 'lastName', 'email', 'city', 'state', 'zip']),
            ...pick(rowToEdit?.businessAddress, ['addressLine1', 'addressLine2']),
        };
    };

    const defaultProps = {
        InputProps: { classes: { input: classes.InputProps } },
        component: TextField,
        margin: 'normal',
        fullWidth: true,
        required: true,
        type: 'text',
    };
    return (
        <Modal
            title={`${rowToEdit ? 'Edit' : 'Add'} organization`}
            customButtons={renderButtons()}
            onCancel={onCancel}
            buttonsPos='start'
            dividers={false}
            isOpen>
            <Container className={classes.container}>
                <div>
                    <Formik
                        initialValues={getInitialValues()}
                        validationSchema={validation}
                        innerRef={myFormRef}
                        onSubmit={onSubmit}>
                        {({ setFieldValue }) => (
                            <Form noValidate>
                                <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <Typography className={classes.typography}>Organization Information</Typography>
                                        <Field {...defaultProps} label='Business name' name='businessName' />
                                        <Field {...defaultProps} label='Address Line 1' name='addressLine1' />
                                        <Field {...defaultProps} label='City' name='city' />
                                    </Grid>
                                    <Grid className={classes.flexBottom} item xs={6}>
                                        <Field
                                            {...defaultProps}
                                            label='Address Line 2'
                                            name='addressLine2'
                                            required={false}
                                        />
                                        <Grid container spacing={3}>
                                            <Grid item xs={6}>
                                                <Field {...defaultProps} label='State' name='state' />
                                            </Grid>
                                            <Grid item xs={6}>
                                                <Field {...defaultProps} label='Zip Code' name='zip' />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <Typography className={classes.typography}>
                                            Administrator Information
                                        </Typography>
                                        <Field {...defaultProps} label='First Name' name='firstName' />
                                        <Field {...defaultProps} label='Last Name' name='lastName' />
                                        <Field {...defaultProps} label='Email Address' name='email' />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography className={clname([classes.typography], [classes.typographyKey])}>
                                            Master Key Information
                                        </Typography>
                                        <div className={classes.masterKey}>
                                            <Field
                                                {...defaultProps}
                                                label='Master Key'
                                                name='masterKey'
                                                inputRef={keyRef}
                                            />
                                            <Button
                                                onClick={() => generateKey(setFieldValue)}
                                                className={classes.masterKeyBtn}
                                                variant='contained'
                                                color='primary'>
                                                Generate Master Key
                                            </Button>
                                        </div>
                                    </Grid>
                                </Grid>
                            </Form>
                        )}
                    </Formik>
                </div>
            </Container>
        </Modal>
    );
};

export default AddEditOrganizationModal;
