/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: ForgotPassword
// ====================================================

export interface ForgotPassword_forgotPassword {
  __typename: "SuccessFailureResponse";
  ok: boolean;
}

export interface ForgotPassword {
  forgotPassword: ForgotPassword_forgotPassword;
}

export interface ForgotPasswordVariables {
  email: string;
}
