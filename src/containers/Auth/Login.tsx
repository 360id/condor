import { Button, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { TextField } from 'formik-material-ui';
import { Formik, Field, Form } from 'formik';
import { Link, useHistory } from 'react-router-dom';
import * as Yup from 'yup';
import React from 'react';
import { useAuth } from '../../auth';
import { useMutation } from '@apollo/client';
import { LOGIN } from '../../graphql/mutations/login';
import { CURRENT_USER } from '../../graphql/queries/currentUser';

const useStyles = makeStyles(theme => ({
    root: {
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        textTransform: 'none',
    },
    textLabel: {
        color: '#3894F0',
    },
    textInput: {
        backgroundColor: 'transparent',
        color: '#000',
        '&:before': {
            borderBottomColor: '#3894F0',
        },
        '&:hover:not(.Mui-disabled):before': {
            borderBottomColor: '#3894F0',
        },
    },
    orLabel: {
        textAlign: 'center',
        marginBottom: '5px',
        color: '#fff',
    },
    linkWrap: {
        textAlign: 'center',
    },
    link: {
        color: '#368EE6',
    },
}));

const LoginValidations = Yup.object().shape({
    email: Yup.string().email('Invalid Email!').required('Required!'),
    password: Yup.string()
        .required('Required!')
        .min(8, 'Password is too short - should be 8 chars minimum!')
        .matches(/^[A-Za-z0-9_]+$/, 'Password can only contain Latin letters and numbers!'),
});

type LoginValues = {
    email: string;
    password: string;
};

const Login: React.FC = () => {
    const classes = useStyles();
    const history = useHistory();
    const [login, { loading, error }] = useMutation(LOGIN, {
        update: (cache, { data: { login } }) =>
            cache.writeQuery({
                query: CURRENT_USER,
                data: { currentUser: login.user },
            }),
    });

    const onSubmit = async (values: LoginValues) => {
        await login({ variables: values });
        history.push('/');
    };

    const defaultProps = {
        InputLabelProps: { classes: { root: classes.textLabel } },
        InputProps: { classes: { root: classes.textInput } },
        className: 'fieldWrap',
        component: TextField,
        margin: 'normal',
        fullWidth: true,
        required: true,
    };

    return (
        <Container component='main' className={classes.root}>
            <Formik initialValues={{ email: '', password: '' }} validationSchema={LoginValidations} onSubmit={onSubmit}>
                {() => (
                    <Form className={classes.form} noValidate>
                        <Field
                            {...defaultProps}
                            label='Email'
                            name='email'
                            autoComplete='email'
                            type='email'
                            autoFocus
                        />
                        <Field
                            {...defaultProps}
                            autoComplete='current-password'
                            label='Password'
                            name='password'
                            type='password'
                        />
                        <Link to='/forgot-password' className={classes.link}>
                            Forgot Password?
                        </Link>
                        <Button className={classes.submit} type='submit' fullWidth variant='contained' color='primary'>
                            Login
                        </Button>
                        <div className={classes.orLabel}>or</div>
                        <div className={classes.linkWrap}>
                            <Link to='/register' className={classes.link}>
                                Create an Account
                            </Link>
                        </div>
                    </Form>
                )}
            </Formik>
        </Container>
    );
};

export default Login;
