import { makeStyles } from '@material-ui/core/styles';
import { Button, Container } from '@material-ui/core';
import { useHistory, useParams } from 'react-router';
import toast from 'react-hot-toast';
import { TextField } from 'formik-material-ui';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import React from 'react';
import { gql, useMutation } from '@apollo/client';

type FormValues = {
    password: string;
};

const RESET_PASSWORD = gql`
    mutation ResetPassword($token: String!, $password: String!) {
        resetPassword(input: { token: $token, password: $password }) {
            ok
        }
    }
`;

const useStyles = makeStyles(theme => ({
    root: {
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        textTransform: 'none',
    },
    textLabel: {
        color: '#3894F0',
    },
    textInput: {
        backgroundColor: 'transparent',
        '&:before': {
            borderBottomColor: '#3894F0',
        },
        '&:hover:not(.Mui-disabled):before': {
            borderBottomColor: '#3894F0',
        },
    },
}));

const validation = Yup.object().shape({
    password: Yup.string()
        .required('Required!')
        .min(8, 'Password is too short - should be 8 chars minimum!')
        .matches(/^[A-Za-z0-9_]+$/, 'Password can only contain Latin letters and numbers!'),
    confirmPassword: Yup.string()
        .required('Required!')
        .oneOf([Yup.ref('password'), ''], 'Passwords must match!'),
});

const ResetPassword = React.memo(() => {
    const { resetToken } = useParams<{ resetToken: string }>();
    const [resetPassword] = useMutation(RESET_PASSWORD);
    const history = useHistory();
    const classes = useStyles();

    const onSubmit = async ({ password }: FormValues) => {
        const result = await resetPassword({ variables: { token: resetToken, password } });
        if (result.data.resetPassword.ok) {
            toast.success('Password set successfully!');
            history.push('/login');
        }
    };

    const defaultProps = {
        InputLabelProps: { classes: { root: classes.textLabel } },
        InputProps: { classes: { root: classes.textInput } },
        className: 'fieldWrap',
        component: TextField,
        margin: 'normal',
        fullWidth: true,
        required: true,
    };

    return (
        <Container component='main' className={classes.root}>
            <Formik
                initialValues={{ password: '', confirmPassword: '' }}
                validationSchema={validation}
                onSubmit={onSubmit}>
                {() => (
                    <Form className={classes.form} noValidate>
                        <Field
                            {...defaultProps}
                            autoComplete='current-password'
                            label='Password'
                            name='password'
                            type='password'
                        />
                        <Field
                            {...defaultProps}
                            autoComplete='current-password'
                            label='Confirm password'
                            name='confirmPassword'
                            type='password'
                        />
                        <Button className={classes.submit} type='submit' fullWidth variant='contained' color='primary'>
                            Set Password
                        </Button>
                    </Form>
                )}
            </Formik>
        </Container>
    );
});

export default ResetPassword;
