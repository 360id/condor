import { Grid, Button, Container, InputLabel, Select, MenuItem, FormControl } from '@material-ui/core';
import { Link, useHistory, useLocation, useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
// import { showNotification } from '../../utils';
import { flattenDeep, pick, omit } from 'lodash';
import { TextField } from 'formik-material-ui';
import { Formik, Field, Form } from 'formik';
import React, { memo, ReactElement, useMemo } from 'react';
import * as Yup from 'yup';
import { gql, useMutation } from '@apollo/client';
import { REGISTER, REGISTER_ADMIN } from '../../graphql/mutations/register';
import { CURRENT_USER } from '../../graphql/queries/currentUser';

type RegistrationParams = {
    token?: string | undefined;
};

type Field = {
    [k: string]: string[];
    left: string[];
    right: string[];
};
type FieldScheme = {
    [k: string]: string[];
    admin: string[];
    default: string[];
};
const FIELD_POS_SCHEME: FieldScheme = {
    admin: ['firstName', 'lastName', 'password', 'confirmPassword'],
    default: ['firstName', 'lastName', 'email', 'password', 'confirmPassword'],
};
type FieldKeys = {
    [k: string]: string[];
};
type Values = {
    [k: string]: string;
};

type Body = Values & {
    data: Values;
};

type FieldList = {
    [k: string]: (props?: any) => unknown;
};

const useStyles = makeStyles(theme => ({
    root: {
        // width: '650px',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    textLabel: {
        color: '#3894F0',
    },
    textInput: {
        backgroundColor: 'transparent',
        color: '#333',
        '&:before': {
            borderBottomColor: '#3894F0',
        },
        '&:hover:not(.Mui-disabled):before': {
            borderBottomColor: '#3894F0',
        },
        '& svg': {
            fill: '#3894F0',
        },
    },
    buttonWrap: {
        display: 'flex',
        justifyContent: 'center',
    },
    submit: {
        margin: '24px auto 16px',
        textTransform: 'none',
        width: '280px',
    },
    linkWrap: {
        textAlign: 'center',
    },
    link: {
        color: '#368EE6',
    },
}));

const validationSchema = {
    firstName: Yup.string().required('Required!'),
    lastName: Yup.string().required('Required!'),
    email: Yup.string().email('Invalid Email').required('Required!'),
    organization: Yup.string().required('Required!'),
    masterKey: Yup.string().required('Required!'),
    phoneNumber: Yup.string().required('Required!'),
    mobileProvider: Yup.string().required('Required!'),
    password: Yup.string()
        .required('Required!')
        .min(8, 'Password is too short - should be 8 chars minimum!')
        .matches(/^[A-Za-z0-9_]+$/, 'Password can only contain Latin letters and numbers!'),
    confirmPassword: Yup.string()
        .required('Required!')
        .oneOf([Yup.ref('password'), ''], 'Passwords must match!'),
};

const Registration = memo(() => {
    const history = useHistory();
    const classes = useStyles();
    const { token } = useParams<RegistrationParams>() || {};
    console.log('Token', token);

    const fieldKeys = FIELD_POS_SCHEME[token ? 'admin' : 'default'] || FIELD_POS_SCHEME.default;

    const [register] = useMutation(REGISTER, {
        update: (cache, { data: { register } }) =>
            cache.writeQuery({
                query: CURRENT_USER,
                data: { currentUser: register.user },
            }),
    });

    const [registerAdmin] = useMutation(REGISTER_ADMIN);

    const fieldList: FieldList = useMemo(() => {
        const defaultProps = {
            InputLabelProps: { classes: { root: classes.textLabel } },
            InputProps: { classes: { root: classes.textInput } },
            className: 'fieldWrap',
            component: TextField,
            margin: 'normal',
            fullWidth: true,
            required: true,
        };
        return {
            email: () => fieldList.default({ label: 'Email', name: 'email', autoComplete: 'email', type: 'email' }),
            organization: () => fieldList.default({ label: 'Organization', name: 'organization' }),
            firstName: () => fieldList.default({ label: 'First name', name: 'firstName' }),
            masterKey: () => fieldList.default({ label: 'Master Key', name: 'masterKey' }),
            lastName: () => fieldList.default({ label: 'Last name', name: 'lastName' }),
            confirmPassword: () =>
                fieldList.default({
                    autoComplete: 'current-password',
                    label: 'Confirm password',
                    name: 'confirmPassword',
                    type: 'password',
                }),
            password: () =>
                fieldList.default({
                    autoComplete: 'current-password',
                    label: 'Password',
                    name: 'password',
                    type: 'password',
                }),
            phoneNumber: () => (
                <Grid container spacing={3} key='phoneNumber'>
                    <Grid item xs={12}>
                        <Field {...defaultProps} label='Mobile Number' name='phoneNumber' type='number' />
                    </Grid>
                </Grid>
            ),
            default: (props: any) => <Field {...defaultProps} {...props} key={props.name} />,
        };
    }, [classes]);

    const getInitialValues = () => {
        const values: Values = {};
        fieldKeys.forEach(key => {
            values[key] = '';
        });
        return values;
    };

    const onSubmit = async (values: Values) => {
        const registerResult = token
            ? await registerAdmin({
                  variables: { ...values, token },
              })
            : await register({
                  variables: values,
              });

        if (registerResult) {
            // showNotification({ type: 'success', message: 'Successfully registered.' });
            history.push('/');
        }
    };

    return (
        <Container component='main' className={classes.root}>
            <Formik
                validationSchema={Yup.object().shape(pick(validationSchema, fieldKeys))}
                initialValues={getInitialValues()}
                onSubmit={onSubmit}>
                {props => (
                    <Form className={classes.form} noValidate>
                        {fieldKeys.map(key => fieldList[key] && fieldList[key](props))}
                        <div className={classes.buttonWrap}>
                            <Button
                                className={classes.submit}
                                type='submit'
                                fullWidth
                                variant='contained'
                                color='primary'>
                                Register
                            </Button>
                        </div>
                        <div className={classes.linkWrap}>
                            <Link to='/login' className={classes.link}>
                                Already registered? Login
                            </Link>
                        </div>
                    </Form>
                )}
            </Formik>
        </Container>
    );
});

export default Registration;
