import { makeStyles } from '@material-ui/core/styles';
import { AnimatedSwitch } from '../../components';
import { Container, Grid } from '@material-ui/core';
import ForgotPassword from './ForgotPassword';
import ResetPassword from './ResetPassword';
import Registration from './Registration';
import Login from './Login';
import React from 'react';

const useStyles = makeStyles(() => ({
    root: {
        padding: 0,
        backgroundImage: 'url("/authBackground.png")',
        backgroundSize: 'cover',
        maxWidth: '100%',
    },
    leftBar: {
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgba(27, 21, 117, 0.5)',
    },
    bigLogo: {
        width: '400px',
    },
    rightBar: {
        height: '100vh',
        background: '#fff',
        padding: '0 24px',
    },
}));

const routes = [
    { exact: true, path: '/', component: Login },
    { exact: true, path: '/login', component: Login },
    { exact: true, path: '/register', component: Registration },
    { exact: true, path: '/register/:token', component: Registration },
    { exact: true, path: '/forgot-password', component: ForgotPassword },
    { exact: true, path: '/reset-password/:resetToken', component: ResetPassword },
];

export default React.memo(() => {
    const classes = useStyles();
    return (
        <Container className={classes.root}>
            <Grid container>
                <Grid item xs={8} className={classes.leftBar}>
                    <img src='/bigLogo.png' alt='bigLogo' className={classes.bigLogo} />
                </Grid>
                <Grid item xs={4} className={classes.rightBar}>
                    <AnimatedSwitch routes={routes} isNotFoundRoute />
                </Grid>
            </Grid>
        </Container>
    );
});
