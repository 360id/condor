import { Button, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { TextField } from 'formik-material-ui';
import { Formik, Field, Form } from 'formik';
import { useHistory } from 'react-router';
import toast from 'react-hot-toast';
import * as Yup from 'yup';
import React from 'react';
import { gql, useMutation } from '@apollo/client';

type FormValues = {
    email: string;
};

const FORGOT_PASSWORD = gql`
    mutation ForgotPassword($email: String!) {
        forgotPassword(input: { email: $email }) {
            ok
        }
    }
`;

const useStyles = makeStyles(theme => ({
    root: {
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        textTransform: 'none',
    },
    textLabel: {
        color: '#3894F0',
    },
    textInput: {
        backgroundColor: 'transparent',
        '&:before': {
            borderBottomColor: '#3894F0',
        },
        '&:hover:not(.Mui-disabled):before': {
            borderBottomColor: '#3894F0',
        },
    },
}));

const EmailValidations = Yup.object().shape({
    email: Yup.string().email('Invalid Email!').required('Required!'),
});

const ForgotPassword = React.memo(() => {
    const history = useHistory();
    const classes = useStyles();

    const [forgotPassword] = useMutation(FORGOT_PASSWORD);

    const onSubmit = async ({ email }: FormValues) => {
        const result = await forgotPassword({ variables: { email } });

        if (result.data.forgotPassword.ok) {
            toast.success('Password sent, check your email.');
            history.push('/'); // todo upd url
        }
    };

    return (
        <Container component='main' className={classes.root}>
            <Formik initialValues={{ email: '' }} validationSchema={EmailValidations} onSubmit={onSubmit}>
                {() => (
                    <Form className={classes.form} noValidate>
                        <Field
                            InputLabelProps={{ classes: { root: classes.textLabel } }}
                            InputProps={{ classes: { root: classes.textInput } }}
                            className='fieldWrap'
                            component={TextField}
                            autoComplete='email'
                            margin='normal'
                            label='Email'
                            name='email'
                            fullWidth
                            autoFocus
                            required
                        />
                        <Button className={classes.submit} type='submit' fullWidth variant='contained' color='primary'>
                            Reset Password
                        </Button>
                    </Form>
                )}
            </Formik>
        </Container>
    );
});

export default ForgotPassword;
