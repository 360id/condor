import { AnimatedSwitch, NavigationBar, ErrorBoundary, MainTopBar, Support, Onboarding } from '../../components';
import { AppBar, Container, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// import { useSelector } from 'react-redux';
import MyAccount from './MyAccount';
import { get } from 'lodash';
import React from 'react';

const useStyles = makeStyles(() => ({
    orgNameWrap: {
        padding: '2px 4px',
        fontSize: '22px',
    },
}));

const routes = [
    { exact: true, path: '/', title: 'My Account', component: MyAccount },
    { exact: true, path: '/support', title: 'Support', component: Support },
];

export default React.memo(() => {
    // const { isOnboarding } = useSelector(store => store.auth.user);
    // const organization = useSelector(state => state.organization);

    const isOnboarding = true;
    const organization = {};

    const classes = useStyles();

    const renderHeaderData = () => {
        const currOrg = 'Testing Org';
        return (
            <span className={classes.orgNameWrap}>
                {currOrg ? <span style={{ padding: '0px 15px' }}>|</span> : ''}
                {currOrg}
            </span>
        );
    };

    return (
        <Box>
            <AppBar position='static'>
                <MainTopBar headerData={renderHeaderData()} />
                {isOnboarding && <NavigationBar routes={routes} />}
            </AppBar>
            <Box mt={3}>
                <Container maxWidth={false}>
                    <ErrorBoundary>
                        {isOnboarding ? <AnimatedSwitch routes={routes} isNotFoundRoute /> : <Onboarding />}
                    </ErrorBoundary>
                </Container>
            </Box>
        </Box>
    );
});
