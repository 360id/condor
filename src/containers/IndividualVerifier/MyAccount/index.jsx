import { Card, CardContent, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { MainLayout } from '../../../components';
import React from 'react';

const useStyles = makeStyles(() => ({
    cardRoot: {
        margin: '100px 20px',
        textAlign: 'center',
        height: '200px',
        width: '220px',
        background: '#ebebeb',
        cursor: 'pointer',
        color: '#3894f0',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontWeight: 600,
    },
    img: {
        width: '110px',
    },
}));

const MENU_ITEMS = [
    {
        title: 'IDENTITY CARD',
        icon: '/id.svg',
    },
    {
        title: 'PASSPORT',
        icon: '/passport.svg',
    },
    {
        title: 'PHOTO',
        icon: '/profile.svg',
    },
];

export default React.memo(() => {
    const classes = useStyles();
    return (
        <MainLayout isDefaultPadding>
            {MENU_ITEMS.map(({ title, icon }) => (
                <Card className={classes.cardRoot} key={title}>
                    <CardContent>
                        <img src={icon} alt={icon} className={classes.img} />
                        <Typography className={classes.title}>{title}</Typography>
                    </CardContent>
                </Card>
            ))}
        </MainLayout>
    );
});
