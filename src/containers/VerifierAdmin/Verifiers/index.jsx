import { MainLayout, VerifiersList } from '../../../components';
import React from 'react';

export default React.memo(() => {
    return (
        <MainLayout isDefaultPadding>
            <VerifiersList />
        </MainLayout>
    );
});
