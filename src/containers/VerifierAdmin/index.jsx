import { AnimatedSwitch, NavigationBar, ErrorBoundary, MainTopBar, Support, Onboarding } from '../../components';
import { AppBar, Container, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Subscriptions from './Subscriptions';
// import { useSelector } from 'react-redux';
import Dashboard from './Dashboard';
import Verifiers from './Verifiers';
import { get } from 'lodash';
import React from 'react';

const useStyles = makeStyles(() => ({
    orgNameWrap: {
        padding: '2px 4px',
        fontSize: '22px',
    },
}));

const routes = [
    { exact: true, path: '/', title: 'Dashboard', component: Dashboard },
    { exact: true, path: '/verifiers', title: 'Verifiers', component: Verifiers },
    // { exact: true, path: '/subscriptions', title: 'Subscriptions', component: Subscriptions },
    { exact: true, path: '/support', title: 'Support', component: Support },
];

export default React.memo(() => {
    // const { isFinichedOnboading } = useSelector(store => store.auth.user); // todo use valid field
    // const organization = useSelector(state => state.organization);
    const { isFinichedOnboading } = {};
    const classes = useStyles();

    const renderHeaderData = () => {
        // const currOrg = get(organization, 'organization.businessName', '');
        const currOrg = 'test';
        return (
            <span className={classes.orgNameWrap}>
                {currOrg ? <span style={{ padding: '0px 15px' }}>|</span> : ''}
                {currOrg}
            </span>
        );
    };

    return (
        <Box>
            <AppBar position='static'>
                <MainTopBar headerData={renderHeaderData()} />
                {!isFinichedOnboading && <NavigationBar routes={routes} />}
            </AppBar>
            <Box mt={3}>
                <Container maxWidth={false}>
                    <ErrorBoundary>
                        {!isFinichedOnboading ? <AnimatedSwitch routes={routes} isNotFoundRoute /> : <Onboarding />}
                    </ErrorBoundary>
                </Container>
            </Box>
        </Box>
    );
});
