import { MainLayout, Chart, LoadingCircle } from '../../../components';
// import organizationSlice from '../../../slices/organization';
// import { useDispatch, useSelector } from 'react-redux';
// import customerSlice from '../../../slices/customer';
// import verifierSlice from '../../../slices/verifier';
import { Grid } from '@material-ui/core';
import React, { useEffect } from 'react';

const xLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October'];

export default React.memo(() => {
    // const { verifierMetricsLoading, verifierMetricsList } = useSelector(state => state.verifier);
    // const { usersMetricsLoading, usersMetricsList } = useSelector(state => state.organization);
    const { verifierMetricsLoading, verifierMetricsList } = {};
    const { usersMetricsLoading, usersMetricsList } = {};
    // const dispatch = useDispatch();

    // useEffect(() => {
    //     dispatch(organizationSlice.thunks.getUsersMetrics());
    //     dispatch(customerSlice.thunks.getCustomerMetrics());
    //     dispatch(verifierSlice.thunks.getVerifierMetrics());
    // }, [dispatch]);

    const charts = [
        {
            datasetProps: { backgroundColor: '#a5c4fc' },
            title: 'Verifiers member metrics',
            isLoading: verifierMetricsLoading,
            data: verifierMetricsList,
        },
        {
            datasetProps: { backgroundColor: '#8EDFFF' },
            title: 'User scans metrics',
            isLoading: usersMetricsLoading,
            data: usersMetricsList,
            type: 'horizontalBar',
        },
    ];

    return (
        <MainLayout isDefaultPadding>
            {charts.map(({ isLoading, title, data, ...rest }, ind) => (
                <Grid item xs={6} key={`key-${ind}`}>
                    {isLoading ? (
                        <LoadingCircle height='300px' />
                    ) : (
                        <Chart simpleData={data} xLabels={xLabels} {...rest} />
                    )}
                </Grid>
            ))}
        </MainLayout>
    );
});
