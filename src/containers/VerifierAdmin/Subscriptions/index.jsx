import { Table, TableHeader, MainLayout, LoadingCircle, StripeModal } from '../../../components';
import { Paper, Grid, Typography, Switch } from '@material-ui/core';
// import subscriptionSlice from '../../../slices/subscription';
// import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect, useState } from 'react';

const ACTIVE_SUBSRCR_ID = 123; // todo remove (get from BE)

const SUBSC_SELECTED_COLOR = {
    123: '#3fe5ef', // todo use valid ids (maybe change to names?)
    234: '#3e50b4',
    345: '#c89e2e',
};

const useStyles = makeStyles(() => ({
    title: {
        marginBottom: '10px',
    },
    selectedRowName: {
        fontSize: '18px',
    },
}));

export default React.memo(() => {
    const selectedRowColor = SUBSC_SELECTED_COLOR[ACTIVE_SUBSRCR_ID];
    // const subscription = useSelector(state => state.subscription);
    const subscription = {};
    const [stripeModal, $stripeModal] = useState(false);
    const [rowToEdit, $rowToEdit] = useState(null);
    const classes = useStyles();
    // const dispatch = useDispatch();

    // useEffect(() => {
    //     dispatch(subscriptionSlice.thunks.getSubscriptionsList());
    //     dispatch(subscriptionSlice.thunks.getBillingsList());
    // }, [dispatch]);

    const subscriptionColumns = React.useMemo(
        () => [
            {
                Header: 'Subscription Name',
                accessor: 'name',
                Cell: ({ row }) => (
                    <div>
                        <div
                            className={classes.selectedRowName}
                            style={row.original.id === ACTIVE_SUBSRCR_ID ? { color: selectedRowColor } : {}}>
                            {row.original.name}
                        </div>
                        {row.original.description}
                    </div>
                ),
            },
            {
                Header: 'Annual Fee',
                accessor: 'annual',
            },
            {
                Header: 'Payment Cycle',
                accessor: 'cycle',
            },
            {
                Header: 'Auto-Renew',
                accessor: 'renew',
                Cell: ({ row }) =>
                    row.original.renew ? (
                        <Switch inputProps={{ 'aria-label': 'primary checkbox' }} checked color='primary' />
                    ) : null,
            },
        ],
        [classes, selectedRowColor],
    );

    const billingColumns = React.useMemo(
        () => [
            {
                Header: 'Credit Card Name',
                accessor: 'name',
            },
            {
                Header: '',
                accessor: 'primary',
            },
        ],
        [],
    );

    const subscriptionActions = {
        Edit: row => console.log('Edit', { row }),
        Delete: row => ({
            confirm: `Are you sure you want to delete ${row.original.name}`,
            func: () => console.log('Delete', { row }),
        }),
    };

    const onAddEditStripe = row => {
        $rowToEdit(row);
        $stripeModal(!stripeModal);
    };

    const billingActions = {
        Edit: row => console.log('Edit', { row }),
        Delete: row => ({
            confirm: `Are you sure you want to delete ${row.original.name}`,
            func: () => console.log('Delete', { row }),
        }),
    };

    return (
        <MainLayout isDefaultPadding>
            <StripeModal
                onOk={() => onAddEditStripe()}
                onCancel={onAddEditStripe}
                isOpen={stripeModal}
                rowToEdit={rowToEdit}
            />
            <Grid item xs={12}>
                <Typography variant='h4' className={classes.title}>
                    Subscription Overview
                </Typography>
                <Paper>
                    {subscription.subscriptionsListLoading ? (
                        <LoadingCircle height='300px' />
                    ) : (
                        <Table
                            data={subscription.subscriptionsList}
                            selectedRowColor={selectedRowColor}
                            selectedRowId={ACTIVE_SUBSRCR_ID}
                            columns={subscriptionColumns}
                            actions={subscriptionActions}
                            defaultPageCount={10}
                            defaultPageIndex={0}
                            defaultPageSize={10}
                        />
                    )}
                </Paper>
            </Grid>
            <Grid item xs={12}>
                <TableHeader title='Billing Methods' onAdd={onAddEditStripe} onAddLabel='Add Billing Method' />
                <Paper>
                    {subscription.billingsListLoading ? (
                        <LoadingCircle height='300px' />
                    ) : (
                        <Table
                            data={subscription.billingsList}
                            columns={billingColumns}
                            actions={billingActions}
                            defaultPageCount={10}
                            defaultPageIndex={0}
                            defaultPageSize={10}
                            selectedRowId={1}
                        />
                    )}
                </Paper>
            </Grid>
        </MainLayout>
    );
});
