import { ErrorBoundary, MainTopBar } from '../../components';
import { AppBar, Container, Box } from '@material-ui/core';
import React from 'react';

export default React.memo(() => {
    return (
        <Box>
            <AppBar position='static'>
                <MainTopBar />
            </AppBar>
            <Box mt={3}>
                <Container maxWidth={false}>
                    <ErrorBoundary>REGULAR USER</ErrorBoundary>
                </Container>
            </Box>
        </Box>
    );
});
