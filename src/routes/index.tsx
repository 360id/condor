import { Route, Switch, useLocation } from 'react-router-dom';
import { Toaster } from 'react-hot-toast';
import { Error404, LoadingScreen, Error440 } from '../components';
import { useQuery, gql } from '@apollo/client';

import React from 'react';

import getRoutes from './getRoutes';
import '../index.css';
import { CURRENT_USER } from '../graphql/queries/currentUser';

const routes: React.FC = () => {
    const location = useLocation();
    const { loading, error, data } = useQuery(CURRENT_USER);

    if (loading) {
        return <LoadingScreen isFullHeight noSpacing />;
    }

    return (
        <>
            <Switch location={location}>
                {getRoutes({ user: data?.currentUser })}
                <Route component={Error404} />
            </Switch>
            <Toaster />
        </>
    );
};

export default routes;
export { routes };
