import makeLazyComponent from '../utils/makeLazyComponent';
import { PrivateRoute } from '../components';
import { Route, useLocation } from 'react-router-dom';
import { USER } from '../constants';
import React from 'react';

type LazyPages = {
    [k: string]: React.ReactElement;
};

const pagesList = ['Auth', 'SuperAdmin', 'VerifierAdmin', 'RegularUser', 'IndividualVerifier'];

const lazyPages: LazyPages = pagesList.reduce(
    (pageComps, page) => ({
        [page]: makeLazyComponent(() => import(`../containers/${page}`)),
        ...pageComps,
    }),
    {},
);

type User = {
    role: string;
};

type Props = {
    user?: User;
};

const getRoutes: React.FC<Props> = ({ user }: Props): React.ReactElement => {
    const isSignedIn = !!user;
    const role = user?.role || 'user';

    console.log('Role', role, 'User', user);
    const pagesList = {
        [USER.ROLES.INDIVIDUAL_VERIFIER]: lazyPages.IndividualVerifier,
        [USER.ROLES.ADMIN_VERIFIER]: lazyPages.VerifierAdmin,
        [USER.ROLES.SUPER_ADMIN]: lazyPages.SuperAdmin,
        [USER.ROLES.USER]: lazyPages.RegularUser,
    };

    const protectedRoutes = [
        { auth: true, exact: false, path: '/', Component: pagesList[role] || pagesList[USER.ROLES.USER] },
    ];
    const defaultsRoutes = [{ auth: false, exact: false, path: '/', Component: lazyPages.Auth }];

    const resRoutes = isSignedIn ? protectedRoutes : defaultsRoutes;

    return (
        <>
            {resRoutes.map(({ auth = true, exact = true, path, Component }) => {
                const RouteComponent = auth ? PrivateRoute : Route;
                return (
                    <RouteComponent key={path} exact={exact} path={path}>
                        {Component}
                    </RouteComponent>
                );
            })}
        </>
    );
};

export default getRoutes;
