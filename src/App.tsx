import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { LoadingScreen } from './components';
import React, { Suspense } from 'react';
import Routes from './routes';
import './index.css';

const theme = createMuiTheme({
    typography: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: 12.5,
    },
    shape: {
        borderRadius: 8,
    },
});

const App: React.FunctionComponent = () => (
    <>
        <CssBaseline />
        <MuiThemeProvider theme={theme}>
            <Suspense fallback={<LoadingScreen isFullHeight noSpacing />}>
                <Routes />
            </Suspense>
        </MuiThemeProvider>
    </>
);

export default App;
