const ROLES = {
    INDIVIDUAL_VERIFIER: 'INDIVIDUAL_VERIFIER',
    ADMIN_VERIFIER: 'ADMIN_VERIFIER',
    SUPER_ADMIN: 'SUPER_ADMIN',
    USER: 'USER',
};

const MOBILE_PROVIDERS = [
    { label: 'USA', value: 'usa' },
    { label: 'Verizon', value: 'verizon' },
];

export default { ROLES, MOBILE_PROVIDERS };
