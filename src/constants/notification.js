const REPEATS_LIST = [
    { label: 'Daily', value: 'daily' },
    { label: 'Weekly', value: 'weekly' },
    { label: 'Monthly', value: 'monthly' },
    { label: 'Yearly', value: 'yearly' },
    { label: 'One time', value: 'onetime' },
];

const REPEATS_LABELS = {
    recurring: 'Recurring',
    onetime: 'One time',
    monthly: 'Monthly',
    weekly: 'Weekly',
    yearly: 'Yearly',
    daily: 'Daily',
};

const AUDIENCES_LIST = [
    { label: 'Regular users', value: 'users' },
    { label: 'Admin verifiers', value: 'admin_verifiers' },
    { label: 'Individual verifiers', value: 'individual_verifiers' },
    { label: 'All users', value: 'all_users' },
];

export default { REPEATS_LIST, REPEATS_LABELS, AUDIENCES_LIST };
