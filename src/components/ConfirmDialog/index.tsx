import { DialogActions, DialogContent, Button, Dialog } from '@material-ui/core';
import React from 'react';

interface ConfirmDialogProps {
    confirm: string;
    callbackFn?: () => void;
    onClose: () => void;
    open: boolean;
}
const ConfirmDialog: React.FC<ConfirmDialogProps> = ({ confirm, callbackFn, onClose, open }) => {
    if (!confirm) return null;
    return (
        <Dialog open={open} onClose={onClose} aria-labelledby='confirm-dialog'>
            <DialogContent>{confirm}</DialogContent>
            <DialogActions>
                <Button size='small' variant='contained' onClick={onClose}>
                    No
                </Button>
                <Button
                    size='small'
                    variant='contained'
                    color='primary'
                    onClick={() => {
                        callbackFn && callbackFn();
                        onClose();
                    }}>
                    Yes
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default ConfirmDialog;
