import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import PropTypes from 'prop-types';
import clname from 'classnames';
import React from 'react';

const useStyles = makeStyles(() => ({
    fullHeight: {
        minHeight: '100vh',
    },
    defaultPadding: {
        padding: '20px 80px',
    },
}));

interface MainLayoutProps {
    isFullHeight?: boolean;
    isDefaultPadding?: boolean;
    noSpacing?: boolean;
    className?: string;
    children?: React.ReactChild;
}

const MainLayout: React.FC<MainLayoutProps> = ({
    children,
    isFullHeight,
    isDefaultPadding,
    noSpacing,
    className,
    ...rest
}) => {
    const { fullHeight, defaultPadding } = useStyles();
    return (
        <Grid
            className={clname([className], { [fullHeight]: isFullHeight, [defaultPadding]: isDefaultPadding })}
            spacing={noSpacing ? 0 : 3}
            alignItems='center'
            justify='center'
            container
            {...rest}>
            {children}
        </Grid>
    );
};

MainLayout.propTypes = {
    children: PropTypes.any.isRequired,
};

export default MainLayout;
