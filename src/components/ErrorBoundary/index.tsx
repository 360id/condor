import React from 'react';

import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import Box from '@material-ui/core/Box';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';

interface GeneralErrorProps {
    title?: string;
    error: Error | undefined;
}
const GeneralError = ({ title, error }: GeneralErrorProps) => {
    return (
        <Box mt={2}>
            <Card>
                <CardHeader title='An error occurred displaying this page. :(' />
                <CardContent>{error?.message}</CardContent>
            </Card>
        </Box>
    );
};

interface Props {
    children: React.ReactNode;
}

interface State {
    hasError: boolean;
    error: Error | undefined;
}

export default class ErrorBoundary extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            hasError: false,
            error: undefined,
        };
    }

    static getDerivedStateFromError(error: Error): State {
        return { hasError: true, error };
    }

    render(): React.ReactNode {
        const { children } = this.props;
        const { hasError, error } = this.state;

        if (hasError) {
            return <GeneralError error={error} />;
        }

        return children;
    }
}
