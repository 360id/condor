import {
    DialogContent as MuiDialogContent,
    DialogActions as MuiDialogActions,
    DialogTitle as MuiDialogTitle,
    Typography,
    IconButton,
    Dialog,
    Button,
} from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';
import React from 'react';
import { JsxElement } from 'typescript';

const DialogTitle = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
        marginTop: '10px',
    },
    title: {
        paddingLeft: '24px',
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
}))(props => {
    const { children } = props;
    return (
        <MuiDialogTitle disableTypography>
            <Typography variant='h6'>{children}</Typography>
            {/* {onClose ? (
                <IconButton aria-label='close'>
                    <CloseIcon />
                </IconButton>
            ) : null} */}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        padding: theme.spacing(1),
        marginBottom: '20px',
        margin: 0,
    },
}))(MuiDialogActions);

const useStyles = makeStyles(() => ({
    cancelBtn: {
        marginRight: '16px',
    },
}));

interface CustomModalProps {
    children?: React.ReactChild;
    onClose?: () => void;
    isOpen: boolean;
    customButtons?: JSX.Element;
    onCancelLabel?: HTMLElement;
    onOkLoading?: boolean;
    onOkLabel?: HTMLElement;
    dividers?: boolean;
    title?: string;
    onOk?: () => void;
    buttonsPos?: string;
    onCancel?: () => void;
    // children: Element;
    // title: string;
    // customButtons: Element;
    // onCancel: (row: OrganizationData) => void;
    // buttonsPos: string;
    // dividers: false;
    // isOpen: true;
}

const CustomModal: React.FC<CustomModalProps> = ({
    customButtons,
    onCancelLabel,
    onOkLoading,
    buttonsPos,
    onOkLabel,
    dividers,
    children,
    onCancel,
    isOpen,
    title,
    onClose,
    onOk,
    ...rest
}) => {
    const classes = useStyles();
    return (
        <Dialog maxWidth='md' onClose={onCancel} open={isOpen} {...rest}>
            {title && <DialogTitle>{title}</DialogTitle>}
            <DialogContent dividers={dividers}>{children}</DialogContent>
            <DialogActions>
                {customButtons || (
                    <div>
                        <Button onClick={onCancel} variant='contained' className={classes.cancelBtn}>
                            {onCancelLabel || 'Cancel'}
                        </Button>
                        <Button onClick={onOk} color='primary' variant='contained' disabled={onOkLoading}>
                            {onOkLoading ? 'Loading...' : onOkLabel || 'Save'}
                        </Button>
                    </div>
                )}
            </DialogActions>
        </Dialog>
    );
};

export default CustomModal;
