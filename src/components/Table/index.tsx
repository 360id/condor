/* eslint-disable react/jsx-key */
import {
    Checkbox,
    Table as MaUTable,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
    Menu,
    MenuItem,
    Theme,
} from '@material-ui/core';
import TablePaginationActions from './TablePaginationActions/TablePaginationActions';
import {
    Column,
    usePagination,
    useRowSelect,
    useSortBy,
    useTable,
    TableOptions,
    HeaderProps,
    CellProps,
    Row,
} from 'react-table';
import React, { ChangeEvent, useCallback, useEffect, useState, PropsWithChildren, SyntheticEvent } from 'react';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { makeStyles } from '@material-ui/core/styles';
import { ConfirmDialog, LoadingCircle } from '..';
import { isObject, isEmpty } from 'lodash';
import IndeterminateCheckbox from '../IndetermindateCheckbox';

interface StyleProps {
    selectedRowColor: string;
}

const useActionStyles = makeStyles(() => ({
    moreBtn: {
        cursor: 'pointer',
        position: 'relative',
        top: '2px',
    },
    moreBtnIcon: {
        fill: '#b4b4b4',
    },
}));

interface DeleteAction {
    confirm: string;
}
interface Actions {
    [k: string]: any;
    Delete: DeleteAction;
}

interface ActionsProps {
    actions: Actions;
    row: any;
}

const Actions: React.FC<ActionsProps> = ({ actions, row }) => {
    const [menuOpen, setMenuOpen] = useState<boolean>(false);
    const [confirmProps, $confirmProps] = useState({});
    const classes = useActionStyles();
    const [menuAnchor, setMenuAnchor] = useState<HTMLElement | null>(null);
    const [confirmDialogOpen, setConfirmDialogOpen] = useState<any>({});

    const onClose = useCallback(() => setMenuOpen(false), []);
    const handleClick = useCallback(
        key => {
            console.log('I am not clicked, 1', key, row);
            const obj = actions[key](row);
            console.log('Object', obj);
            if (isObject(obj)) setConfirmDialogOpen(obj);
            onClose();
        },
        [actions, row, onClose],
    );

    const handleMenuClick = (e: SyntheticEvent<HTMLElement>) => {
        setMenuOpen(!menuOpen);
        setMenuAnchor(e.target as HTMLElement);
    };

    return (
        <>
            <ConfirmDialog
                open={!isEmpty(confirmDialogOpen)}
                confirm={confirmDialogOpen.confirm}
                onClose={() => setConfirmDialogOpen({})}
                callbackFn={confirmDialogOpen.removeRow}
            />
            <span className={classes.moreBtn} onClick={handleMenuClick}>
                <MoreVertIcon className={classes.moreBtnIcon} />
            </span>
            <Menu open={menuOpen} anchorEl={menuAnchor} onClose={onClose}>
                {Object.keys(actions).map(key => (
                    <MenuItem onClick={() => handleClick(key)} key={key}>
                        {key}
                    </MenuItem>
                ))}
            </Menu>
        </>
    );
};

const useTableStyles = makeStyles<Theme, StyleProps>(() => ({
    selectedRow: {
        background: '#EEF3F4',
        '& td': {
            fontWeight: 700,
            position: 'relative',
        },
    },
    selectedRowDiv: {
        background: props => props.selectedRowColor || '#3e50b4',
        position: 'absolute',
        height: '100%',
        width: '5px',
        left: 0,
        top: 0,
    },
    centerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '10px',
        display: 'flex',
        height: '100px',
    },
}));

export interface TableProps<T extends Record<string, unknown>> extends TableOptions<T> {
    actions?: any;
}

function Table<T extends Record<string, unknown>>(props: PropsWithChildren<TableProps<T>>): React.ReactElement {
    const {
        columns,
        data,
        defaultPageCount = 0,
        defaultPageIndex = 0,
        defaultPageSize = 25,
        onPageIndexChange,
        onPageSizeChange,
        manualPagination = false,
        manualSortBy = false,
        onRowSelection,
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        onSort = () => {},
        selectedRowId,
        selectedRowColor,
        actions,
        loading,
        noDataMessage,
    } = props;

    const classes = useTableStyles({ selectedRowColor });
    const checboxCol = onRowSelection
        ? [
              {
                  id: 'selection',
                  Header: ({ getToggleAllRowsSelectedProps }: HeaderProps<T>) => (
                      <div>
                          <Checkbox {...getToggleAllRowsSelectedProps()} />
                      </div>
                  ),
                  Cell: ({ row }: CellProps<T>) => (
                      <div>
                          <Checkbox {...row.getToggleRowSelectedProps()} />
                      </div>
                  ),
              },
          ]
        : [];

    const actionCol = actions
        ? [
              {
                  id: 'action',
                  Header: () => 'Action',
                  Cell: ({ row }: CellProps<T>) => <Actions {...{ actions, row }} />,
              },
          ]
        : [];

    const {
        getTableProps,
        headerGroups,
        prepareRow,
        page,
        gotoPage,
        setPageSize,
        selectedFlatRows,
        state: { sortBy, pageIndex, pageSize, selectedRowIds },
    } = useTable(
        {
            columns,
            data,
            pageCount: defaultPageCount,
            initialState: {
                pageSize: defaultPageSize,
                pageIndex: defaultPageIndex,
            },
            manualSortBy,
        },
        useSortBy,
        usePagination,
        useRowSelect,
        hooks => {
            hooks.allColumns.push(cols => [...checboxCol, ...cols, ...actionCol]);
        },
    );

    useEffect(() => {
        if (onSort) onSort(sortBy);
        return;
    }, [onSort, sortBy]);

    useEffect(() => {
        if (onRowSelection) {
            onRowSelection(selectedRowIds, selectedFlatRows);
        }
    }, [onRowSelection, selectedFlatRows, selectedRowIds]);

    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent> | null,
        newPage: number,
    ) => {
        gotoPage(Number(newPage));
        if (manualPagination) {
            onPageIndexChange(Number(newPage));
        }
    };

    const handleChangeRowsPerPage = (event: ChangeEvent<HTMLInputElement>) => {
        setPageSize(Number(event.target.value));
        if (manualPagination) {
            onPageSizeChange(Number(event.target.value));
        }
    };

    // Render the UI for your table
    return (
        <TableContainer>
            <MaUTable {...getTableProps()}>
                <TableHead>
                    {headerGroups.map(headerGroup => (
                        <TableRow {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                // eslint-disable-next-line react/jsx-key
                                <TableCell
                                    {...(column.id === 'selection'
                                        ? column.getHeaderProps()
                                        : column.getHeaderProps(column.getSortByToggleProps()))}>
                                    {column.render('Header')}
                                    {column.id !== 'selection' ? (
                                        <TableSortLabel
                                            active={column.isSorted}
                                            // react-table has a unsorted state which is not treated here
                                            direction={column.isSortedDesc ? 'desc' : 'asc'}
                                        />
                                    ) : null}
                                </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableHead>
                <TableBody>
                    {!loading && page.length ? (
                        page.map((row, i) => {
                            console.log('Row', row);
                            prepareRow(row);
                            const isSelected = selectedRowId && selectedRowId === row.original.id;
                            return (
                                <TableRow {...row.getRowProps()} className={isSelected ? classes.selectedRow : ''}>
                                    {row.cells.map((cell, ind) => (
                                        <TableCell {...cell.getCellProps()}>
                                            {isSelected && ind === 0 && <div className={classes.selectedRowDiv} />}
                                            {cell.render('Cell')}
                                        </TableCell>
                                    ))}
                                </TableRow>
                            );
                        })
                    ) : (
                        <TableRow>
                            <TableCell colSpan={columns.length + 1}>
                                <div className={classes.centerContainer}>
                                    {loading ? <LoadingCircle size={5} /> : noDataMessage || 'No data'}
                                </div>
                            </TableCell>
                        </TableRow>
                    )}
                </TableBody>
                {/* <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 25, { label: 'All', value: data.length }]}
                            colSpan={columns.length + 1}
                            count={data.length}
                            rowsPerPage={pageSize}
                            page={pageIndex}
                            SelectProps={{
                                inputProps: { 'aria-label': 'rows per page' },
                                native: true,
                            }}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                        />
                    </TableRow>
                </TableFooter> */}
            </MaUTable>
        </TableContainer>
    );
}

export default Table;
