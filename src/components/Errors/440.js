import { Grid, Typography } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { MainLayout } from '../';

export default React.memo(({ from }) => {
    const [shouldRedirect, setShouldRedirect] = useState(false);

    // let user see the logout message before redirecting to /login
    useEffect(() => {
        setTimeout(() => setShouldRedirect(true), 2 * 1000);
    }, []);

    if (shouldRedirect) {
        const redirecToConfig = { pathname: '/login', state: { from } };
        return <Redirect to={redirecToConfig} />;
    }

    return (
        <MainLayout className='error-page'>
            <Grid container justify='center' alignItems='center'>
                <Grid item>
                    <Typography variant='h5' align='center' className='message'>
                        Your session has expired. We will redirect you to login page.
                    </Typography>
                </Grid>
            </Grid>
        </MainLayout>
    );
});
