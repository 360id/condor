import { Grid, Link, Typography } from '@material-ui/core';
import { Link as BrowserLink } from 'react-router-dom';
import { MainLayout } from '../';
import React from 'react';

export default React.memo(() => {
    return (
        <MainLayout className='error-page'>
            <Grid container justify='center' alignItems='center'>
                <Grid item>
                    <Typography variant='h5' align='center' className='message'>
                        Page Not Found. Back to
                        <Link component={BrowserLink} to='/'>
                            Homepage
                        </Link>
                    </Typography>
                </Grid>
            </Grid>
        </MainLayout>
    );
});
