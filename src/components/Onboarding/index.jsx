import { Grid, Typography, Button, Step, Stepper, StepLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import { LICENCE } from '../../constants';
import React, { useState } from 'react';
// import verifierSlice from '../../slices/verifier';
import { push } from 'react-router-redux';
// import { useDispatch, useSelector } from 'react-redux';
import { MainLayout } from '../';
import clname from 'classnames';

const useStyles = makeStyles(() => ({
    contentWrap: {
        marginTop: '100px',
        height: '280px',
        textAlign: 'center',
    },
    title: {
        fontSize: '24px',
        marginBottom: '25px',
        fontWeight: 600,
        fontFamily: 'Raleway',
    },
    line: {
        width: '30px',
        height: '2px',
        margin: '5px auto',
        background: '#3894f0',
    },
    welcome: {
        paddingBottom: '20px',
    },
    bold: {
        fontWeight: 600,
    },
    description: {
        fontSize: '14px',
    },
    licenceWrap: {
        display: 'flex',
        justifyContent: 'center',
    },
    licence: {
        border: '1px solid #e2e2e2',
        padding: '10px',
        textAlign: 'left',
        borderRadius: '5px',
        maxWidth: '700px',
        height: '210px',
        overflow: 'auto',
    },
    successImg: {
        width: '130px',
        height: '130px',
        marginTop: '30px',
    },
    stepsWrap: {
        background: 'transparent',
        padding: '24px 24px 24px 32px',
        width: '400px',
        margin: '0 auto',
    },
    btnsWrap: {
        display: 'flex',
        justifyContent: 'center',
    },
    nextButton: {
        textTransform: 'none',
        padding: '6px 70px',
    },
    nextButtonPadding: {
        padding: '6px 30px',
    },
    backButton: {
        textTransform: 'none',
        background: 'transparent',
        marginLeft: '16px',
    },
    btnIcon: {
        fontSize: '16px',
        marginRight: '8px',
    },
}));

export default React.memo(() => {
    const [activeStep, $activeStep] = useState(0);
    // const dispatch = useDispatch();
    // const { user } = useSelector(state => state.auth);
    const user = {};
    const classes = useStyles();

    const STEPS_DATA = {
        0: {
            btnLabel: 'Start',
            title: 'Welcome',
            content: (
                <span>
                    <Typography component='h1' variant='h6' className={classes.welcome}>
                        Welcome to the 360ID Web Application for: <br />
                        <span className={classes.bold}>company/organization</span>
                    </Typography>
                    <Typography component='h1' variant='h6' className={classes.description}>
                        The following screens will take you to the set up process to get you <br /> started with the
                        application.
                    </Typography>
                </span>
            ),
        },
        1: {
            // eslint-disable-next-line react/jsx-key
            btnLabel: [<CheckIcon className={classes.btnIcon} />, 'Accept'],
            title: 'End User Licence Agreement',
            content: (
                <div className={classes.licenceWrap}>
                    <div className={classes.licence}>{LICENCE}</div>
                </div>
            ),
        },
        2: {
            btnLabel: 'Finish',
            title: 'Great!',
            content: (
                <span>
                    <Typography component='h1' variant='h6' className={classes.bold}>
                        Onboarding successfull
                    </Typography>
                    <Typography component='h1' variant='h6' className={classes.description}>
                        You can now start using 360ID
                    </Typography>
                    <img src='/success.svg' alt='success' className={classes.successImg} />
                </span>
            ),
        },
    };

    const handleNext = () => {
        if (activeStep < 2) {
            $activeStep(activeStep + 1);
        } else {
            // dispatch().then(() => dispatch(push('/')));
            // verifierSlice.thunks.addUpdateVerifier({ ...user, isOnboarding: true }, user.organizationId),
        }
    };

    const handlePrev = () => {
        if (activeStep > 0) $activeStep(activeStep - 1);
    };

    const { title, content, btnLabel } = STEPS_DATA[activeStep];
    return (
        <MainLayout isDefaultPadding>
            <Grid item xs={6}>
                <div className={classes.contentWrap}>
                    <Typography component='h1' variant='h4' className={classes.title}>
                        {title}
                        <div className={classes.line} />
                    </Typography>
                    {content}
                </div>
                <Stepper activeStep={activeStep} className={classes.stepsWrap} alternativeLabel>
                    {[...Array(3).keys()].map(key => (
                        <Step key={key}>
                            <StepLabel />
                        </Step>
                    ))}
                </Stepper>
                <div className={classes.btnsWrap}>
                    <Button
                        variant='contained'
                        color='primary'
                        onClick={handleNext}
                        className={clname([classes.nextButton], { [classes.nextButtonPadding]: activeStep === 1 })}>
                        {btnLabel}
                    </Button>
                    {activeStep === 1 && (
                        <Button onClick={handlePrev} variant='contained' className={classes.backButton}>
                            <CloseIcon className={classes.btnIcon} /> Decline
                        </Button>
                    )}
                </div>
            </Grid>
        </MainLayout>
    );
});
