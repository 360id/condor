// import verifierSclice from '../../../slices/verifier';
import { Container, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { TextField } from 'formik-material-ui';
import { Formik, Field, Form, FormikValues } from 'formik';
// import { useDispatch } from 'react-redux';
import React, { useRef } from 'react';
import { Modal } from '../..';
import { pick } from 'lodash';
import * as Yup from 'yup';
import { gql, useMutation, useQuery } from '@apollo/client';
import { RoomTwoTone } from '@material-ui/icons';
import { CURRENT_USER } from '../../../graphql/queries/currentUser';

const CREATE_USER = gql`
    mutation CreateUser($email: String!, $organizationId: String!) {
        createUser(input: { email: $email, organizationId: $organizationId }) {
            email
        }
    }
`;

const UPDATE_USER = gql`
    mutation UpdateUser($id: String!, $email: String, $firstName: String, $lastName: String) {
        updateUser(id: $id, input: { email: $email, firstName: $firstName, lastName: $lastName }) {
            email
        }
    }
`;

const useStyles = makeStyles(() => ({
    firstName: {
        marginTop: '0px',
    },
    addButtonsWrap: {
        padding: '0px 30px',
        width: '100%',
    },
    addButton: {
        textTransform: 'none',
        fontSize: '14px',
        width: '100%',
    },
}));

const validation = Yup.object().shape({
    email: Yup.string().email('Invalid Email').required('Required'),
});

type UserData = {
    email?: string;
    id?: string;
    firstName?: string;
    lastName?: string;
};

type AddEditVerifierModalProps = {
    onCancel: () => void;
    rowToEdit: UserData;
    organizationId: string;
};

const AddEditVerifierModal: React.FC<AddEditVerifierModalProps> = ({ onCancel, rowToEdit, organizationId }) => {
    console.log('Row toEdit', rowToEdit);
    // const dispatch = useDispatch();
    const classes = useStyles();
    const VerifierFormRef = useRef<any>(null);

    const { loading, error, data } = useQuery(CURRENT_USER);
    const [createUser] = useMutation(CREATE_USER);
    const [updateUser] = useMutation(UPDATE_USER);

    console.log('My', VerifierFormRef.current);

    const handleSave = () => {
        console.log('I am clicked');
        VerifierFormRef?.current?.handleSubmit();
    };
    const renderButtons = () => (
        <div className={classes.addButtonsWrap}>
            <Button onClick={handleSave} className={classes.addButton} variant='contained' color='primary'>
                {rowToEdit ? 'Save' : 'Add verifier and send key via email'}
            </Button>
        </div>
    );

    const onSubmit = async (values: FormikValues) => {
        try {
            console.log('...submitting');
            let body: UserData = {
                email: values.email,
            };
            if (rowToEdit) {
                body = { ...body, id: rowToEdit.id, firstName: values.firstName, lastName: values.lastName };
            }

            const result = body.id
                ? await updateUser({
                      variables: {
                          id: body.id,
                          email: body.email,
                          firstName: body.firstName,
                          lastName: body.lastName,
                      },
                  })
                : await createUser({
                      variables: {
                          email: body.email,
                          organizationId: data.currentUser.organizationId,
                      },
                  });
            if (result) onCancel();
        } catch (e) {
            console.log('something happ');
        }
    };

    const getInitialValues = () => {
        if (rowToEdit) return { email: rowToEdit.email };
        return { email: '' };
    };

    const defaultProps = {
        component: TextField,
        margin: 'normal',
        fullWidth: true,
        type: 'text',
    };

    return (
        <Modal
            title={`${rowToEdit ? 'Edit' : 'Add'} Verifier`}
            customButtons={renderButtons()}
            onCancel={onCancel}
            buttonsPos='start'
            dividers={false}
            isOpen>
            <Container>
                <Formik
                    initialValues={getInitialValues()}
                    validationSchema={validation}
                    innerRef={VerifierFormRef}
                    onSubmit={onSubmit}>
                    {() => (
                        <Form noValidate>
                            {rowToEdit ? (
                                <>
                                    <Field {...defaultProps} label='First Name' name='firstName' />
                                    <Field {...defaultProps} label='Last Name' name='lastName' />
                                </>
                            ) : null}
                            <Field {...defaultProps} label='Email Address' name='email' />
                        </Form>
                    )}
                </Formik>
            </Container>
        </Modal>
    );
};

export default AddEditVerifierModal;
