/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetOrganizationUsers
// ====================================================

export interface GetOrganizationUsers_getOrganizationUsers_users {
  __typename: "UserDto";
  id: string;
  email: string;
  role: string;
  firstName: string | null;
  lastName: string | null;
}

export interface GetOrganizationUsers_getOrganizationUsers {
  __typename: "OrgUsersData";
  users: GetOrganizationUsers_getOrganizationUsers_users[];
}

export interface GetOrganizationUsers {
  getOrganizationUsers: GetOrganizationUsers_getOrganizationUsers;
}

export interface GetOrganizationUsersVariables {
  orgId: string;
}
