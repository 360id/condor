// import organizationSlice from '../../slices/organization';
import AddEditVerifierModal from './AddEditVerifierModal';
// import { useDispatch, useSelector } from 'react-redux';
// import verifierSclice from '../../slices/verifier';
import React, { useEffect, useState } from 'react';
import { Table, TableHeader, Status } from '../';
import { Paper, Grid } from '@material-ui/core';
import { useParams } from 'react-router';
import { gql, useQuery, useMutation } from '@apollo/client';
import { useAuth } from '../../auth';
import LoadingScreen from '../LoadingScreen';
import { CURRENT_USER } from '../../graphql/queries/currentUser';

const GET_ORGANIZATION_USERS = gql`
    query GetOrganizationUsers($orgId: String!) {
        getOrganizationUsers(id: $orgId) {
            users {
                id
                email
                role
                firstName
                lastName
            }
        }
    }
`;

const DELETE_USER = gql`
    mutation DeleteUser($id: String!) {
        deleteUser(id: $id)
    }
`;

export default React.memo(() => {
    // const { verifierLoading, verifierData } = useSelector(state => state.organization);
    const { verifierLoading, verifierData } = {};
    const [isAddEditModal, $isAddEditModal] = useState(false);
    // const user = useSelector(store => store.auth.user);
    // const orgUsers = [];
    const [rowToEdit, $rowToEdit] = useState(null);
    // const dispatch = useDispatch();
    const params = useParams();
    const [deleteUser] = useMutation(DELETE_USER);

    // useEffect(() => {
    //     dispatch(organizationSlice.thunks.getVerifiersByOrgId(organizationId));
    // }, [dispatch, organizationId]);

    const columns = React.useMemo(
        () => [
            {
                Header: 'First Name',
                accessor: 'data.firstName',
            },
            {
                Header: 'Last Name',
                accessor: 'data.lastName',
            },
            {
                Header: 'Email',
                accessor: 'email',
            },
            {
                Header: 'Phone',
                accessor: 'mobile',
            },
            {
                Header: 'Individual key',
                accessor: 'individualKey',
            },
            {
                Header: 'Access',
                accessor: 'role',
            },
            {
                Header: 'Status',
                accessor: 'status',
                Cell: ({ value }) => <Status value={value} />,
            },
        ],
        [],
    );

    const onSearch = searchText => {
        // dispatch(organizationSlice.thunks.getVerifiersByOrgId(organizationId, { searchText }));
    };
    // const onSearch = searchText => {
    //     dispatch(organizationSlice.thunks.getVerifiersByOrgId(organizationId, { searchText }));
    // };

    const onAddEdit = row => {
        $rowToEdit(row);
        $isAddEditModal(!isAddEditModal);
    };

    const actions = {
        Edit: row => onAddEdit(row.original),
        Delete: row => ({
            confirm: `Are you sure you want to delete ${row.original.firstName || ''} ${row.original.lastName || ''}?`,
            removeRow: () => deleteUser({ variables: { id: row.original.id } }),
        }),
    };

    const skipCurrentUser = !!params.organizationId;

    const { loading, error, data: { currentUser: { organizationId } = {} } = {} } = useQuery(CURRENT_USER, {
        skip: skipCurrentUser,
    });
    const skip = !organizationId && !params.organizationId;

    const { loading: orgLoading, error: orgError, data: orgData } = useQuery(GET_ORGANIZATION_USERS, {
        skip: skip,
        variables: { orgId: params.organizationId || organizationId },
    });

    console.log('what', orgLoading, orgData);
    if (orgLoading || !orgData) return <LoadingScreen />;

    const orgUsers = orgData.getOrganizationUsers.users;
    console.log('OrgUsers', orgUsers);

    return (
        <Grid item xs={12}>
            {isAddEditModal && (
                <AddEditVerifierModal
                    onCancel={() => onAddEdit()}
                    rowToEdit={rowToEdit}
                    organizationId={organizationId}
                />
            )}
            <TableHeader onSearch={onSearch} onAdd={() => onAddEdit()} onAddLabel='Add Verifier' />
            <Paper>
                <Table data={orgUsers || []} loading={verifierLoading} actions={actions} columns={columns} />
            </Paper>
        </Grid>
    );
});
