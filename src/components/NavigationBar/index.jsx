import { makeStyles } from '@material-ui/core/styles';
import { Link, useLocation } from 'react-router-dom';
import { Tabs, Tab } from '@material-ui/core';
import PropTypes from 'prop-types';
import React from 'react';

const useStyles = makeStyles(() => ({
    tabMenu: {
        flexGrow: 1,
        background: 'rgb(244, 245, 247)',
    },
    singleTab: {
        fontWeight: 600,
        color: '#000',
        textTransform: 'none',
        opacity: 0.5,
    },
}));

const NavigationBar = React.memo(({ routes, className }) => {
    const location = useLocation();
    const classes = useStyles();

    return (
        <Tabs className={className || classes.tabMenu} value={location.pathname} indicatorColor='primary' centered>
            {routes
                .filter(itm => itm.title)
                .map(({ title, path }) => (
                    <Tab
                        component={Link}
                        label={title}
                        value={path}
                        to={path}
                        key={path}
                        className={classes.singleTab}
                    />
                ))}
        </Tabs>
    );
});

NavigationBar.propTypes = {
    routes: PropTypes.array.isRequired,
    className: PropTypes.string,
};

export default NavigationBar;
