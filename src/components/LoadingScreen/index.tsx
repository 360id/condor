import React, { FC } from 'react';
import { LoadingCircle, MainLayout } from '..';
import Grid from '@material-ui/core/Grid';

interface LoadingScreenProps {
    size?: number;
    height?: number;
    isFullHeight?: boolean;
    noSpacing?: boolean;
}

const LoadingScreen: FC<LoadingScreenProps> = ({ size, ...otherProps }) => {
    return (
        <MainLayout {...otherProps}>
            <Grid item>
                <LoadingCircle size={size || 80} />
            </Grid>
        </MainLayout>
    );
};

export default LoadingScreen;
