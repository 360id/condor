import { Paper, Grid, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { TextField } from 'formik-material-ui';
import { Field, Form, Formik } from 'formik';
import { MainLayout } from '../';
// import customerService from '../../services/customerService';
// import { showNotification } from '../../utils';
import * as Yup from 'yup';
import React from 'react';

const LoginValidations = Yup.object().shape({
    email: Yup.string().email('Invalid Email').required('Required'),
    subject: Yup.string().required('Required'),
    message: Yup.string().required('Required'),
});

const useStyles = makeStyles(() => ({
    title: {
        marginBottom: '20px',
    },
    paper: {
        padding: '10px 40px 40px 40px',
    },
    messageInputWrap: {
        marginTop: '30px',
        marginBottom: '20px',
    },
    messageInput: {
        height: '115px !important',
    },
}));

export default React.memo(() => {
    const classes = useStyles();

    const onSubmit = async values => {
        try {
            const body = {
                userEmail: values.email,
                subject: values.subject,
                emailMessage: values.message,
            };
            // const res = await customerService.supportCustomer(body);
            if (res) {
                // showNotification({ type: 'success', message: res.message });
            }
        } catch (e) {
            console.error(e);
        }
    };

    const defaultProps = {
        component: TextField,
        margin: 'normal',
        fullWidth: true,
        required: true,
    };
    return (
        <MainLayout isDefaultPadding>
            <Grid item xs={4}>
                <Typography component='h1' variant='h5' className={classes.title}>
                    Contact Us
                </Typography>
                <Paper className={classes.paper}>
                    <Formik
                        initialValues={{ email: '', subject: '', message: '' }}
                        validationSchema={LoginValidations}
                        onSubmit={onSubmit}>
                        {() => (
                            <Form className={classes.form} noValidate>
                                <Field {...defaultProps} label='Email' name='email' autoComplete='email' type='email' />
                                <Field {...defaultProps} label='Subject' name='subject' />
                                <Field
                                    InputProps={{ classes: { input: classes.messageInput }, multiline: true }}
                                    className={classes.messageInputWrap}
                                    component={TextField}
                                    variant='outlined'
                                    label='Message'
                                    margin='normal'
                                    name='message'
                                    fullWidth
                                    required
                                />
                                <Button
                                    className={classes.submit}
                                    type='submit'
                                    fullWidth
                                    variant='contained'
                                    color='primary'>
                                    Send Message
                                </Button>
                            </Form>
                        )}
                    </Formik>
                </Paper>
            </Grid>
        </MainLayout>
    );
});
