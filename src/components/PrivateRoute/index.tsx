import { Route, Redirect } from 'react-router-dom';
import React from 'react';
import { useAuth } from '../../auth';

type Props = {
    component?: React.FC;
    children?: React.ReactChild;
};
export default React.memo(({ component: _component, children, ...rest }: Props) => {
    const { isSignedIn, signOut } = useAuth();

    if (!isSignedIn) {
        signOut();
        return <Redirect to='/login' />;
    }

    if (_component) {
        // client wants to render Route via component prop
        return <Route {...rest} component={_component} />;
    } else {
        // client wants to render Route via children function
        return <Route {...rest}>{children}</Route>;
    }
});
