import { IconButton, MenuItem, Menu } from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { useHistory } from 'react-router-dom';
import React from 'react';
import { useAuth } from '../../auth';
import { LOGOUT } from '../../graphql/mutations/logout';
import { useMutation } from '@apollo/client';
import { CURRENT_USER } from '../../graphql/queries/currentUser';

export default React.memo(() => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [logout] = useMutation(LOGOUT, {
        update: cache =>
            cache.writeQuery({
                query: CURRENT_USER,
                data: { currentUser: null },
            }),
    });
    const history = useHistory();

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleSignOut = () => {
        logout();
        history.push('/');
    };

    return (
        <>
            <IconButton
                onClick={event => setAnchorEl(event.currentTarget)}
                aria-label='account of current user'
                aria-controls='menu-appbar'
                aria-haspopup='true'
                color='inherit'>
                <AccountCircle fontSize='large' />
            </IconButton>
            <Menu
                id='menu-appbar'
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={Boolean(anchorEl)}
                onClose={handleClose}>
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={handleSignOut}>Logout</MenuItem>
            </Menu>
        </>
    );
});
