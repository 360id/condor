import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { Toolbar } from '@material-ui/core';
import PropTypes from 'prop-types';
import { UserMenu } from '../';
import React from 'react';

const useStyles = makeStyles(() => ({
    toolbarRoot: {
        paddingLeft: '0px',
    },
    left: {
        display: 'flex',
        flexGrow: 1,
    },
    logoWrap: {
        paddingLeft: '24px',
    },
    logoImg: {
        height: '35px',
    },
    topBarNavWrap: {
        display: 'flex',
    },
    arrowBtn: {
        padding: '18px 24px',
        background: 'rgb(86, 100, 210)',
        cursor: 'pointer',
    },
}));

const MainTopBar = React.memo(({ isBackIcon, headerData, isNoUser }) => {
    const history = useHistory();
    const classes = useStyles();

    return (
        <Toolbar className={classes.toolbarRoot}>
            <div className={classes.left}>
                <div className={classes.topBarNavWrap}>
                    {isBackIcon ? (
                        <div className={classes.arrowBtn} onClick={() => history.goBack()}>
                            <ArrowBackIosIcon />
                        </div>
                    ) : (
                        <div className={classes.logoWrap}>
                            <img src='/logo.png' alt='logo' className={classes.logoImg} />
                        </div>
                    )}
                    {headerData || null}
                </div>
            </div>
            {!isNoUser && (
                <div>
                    <UserMenu />
                </div>
            )}
        </Toolbar>
    );
});

MainTopBar.propTypes = {
    isBackIcon: PropTypes.bool,
    headerData: PropTypes.any,
    isNoUser: PropTypes.bool,
};

export default MainTopBar;
