import { OutlinedInput, InputAdornment, Button, Grid, Typography } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Search, Add } from '@material-ui/icons';
import React, { useState, useRef, ChangeEvent, MouseEvent } from 'react';
import { debounce } from 'lodash';
import { TableRowProps } from 'react-table';

const useStyles = makeStyles(() =>
    createStyles({
        TableHeader: {
            marginBottom: '10px',
        },
        buttonsWrap: {
            display: 'flex',
            justifyContent: 'flex-end',
        },
        searchInputWrap: {
            padding: '8px 8px 8px 16px',
        },
        searchInput: {
            fontSize: '14px',
            padding: 0,
        },
        searchIcon: {
            paddingRight: '0px',
            color: '#c0c0c0',
        },
        title: {
            fontSize: '22px',
        },
        addButton: {
            textTransform: 'none',
            marginLeft: '16px',
            fontSize: '14px',
        },
        addIcon: {
            width: '19px',
            height: '19px',
        },
    }),
);

interface TableHeaderProps {
    onAddLabel: string;
    onSearch: (searchText: string) => void;
    title: string;
    onAdd: any;
}

const TableHeader: React.FC<TableHeaderProps> = ({ title, onSearch, onAdd, onAddLabel }) => {
    const [searchText, $searchText] = useState('');
    const classes = useStyles();

    // const debouncedOnSearch = useRef(onSearch && debounce(onSearch, 400));
    const handleSearch = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        $searchText(e.target.value);
        // debouncedOnSearch.current();
    };

    const handleClick = () => {
        onAdd();
    };

    return (
        <Grid container spacing={3} className={classes.TableHeader}>
            <Grid item xs={6}>
                {title && <Typography className={classes.title}>{title}</Typography>}
            </Grid>
            <Grid item xs={6} className={classes.buttonsWrap}>
                <OutlinedInput
                    endAdornment={
                        <InputAdornment className={classes.searchIcon} position='end'>
                            <Search />
                        </InputAdornment>
                    }
                    inputProps={{ className: classes.searchInput }}
                    className={classes.searchInputWrap}
                    placeholder='Search...'
                    onChange={handleSearch}
                    value={searchText}
                    color='primary'
                />

                {
                    <Button className={classes.addButton} variant='contained' onClick={handleClick} color='primary'>
                        <Add className={classes.addIcon} />
                        {onAddLabel}
                    </Button>
                }
            </Grid>
        </Grid>
    );
};

export default TableHeader;
