import React, { FC } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

interface LoadingCircleProps {
    size: number;
    height?: number;
}

const LoadingCircle: FC<LoadingCircleProps> = ({ size = 40, height }) => (
    <Grid style={{ height: height || '100%', width: '100%' }} container alignItems='center' justify='center'>
        <Typography variant='h4' align='center'>
            <CircularProgress size={size} />
        </Typography>
    </Grid>
);

export default LoadingCircle;
