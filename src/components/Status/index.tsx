import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import { makeStyles } from '@material-ui/core/styles';
import clname from 'classnames';
import React from 'react';

const useStyles = makeStyles(() => ({
    statusIcon: {
        width: '14px',
        height: '14px',
        position: 'relative',
        top: '3px',
        left: '-3px',
    },
    statusIconActive: {
        fill: '#2dc756',
    },
    statusIconDraft: {
        fill: '#eab75e',
    },
    statusIconInactive: {
        fill: '#b4b4b4',
    },
}));

interface StatusProps {
    value: string;
}
interface Classes {
    [k: string]: string;
}
const Status: React.FC<StatusProps> = ({ value = 'draft' }) => {
    const classes: Classes = useStyles();
    const status = value.charAt(0).toUpperCase() + value.slice(1);
    return (
        <span>
            <FiberManualRecordIcon className={clname([classes.statusIcon], [classes[`statusIcon${status}`]])} />
            {status}
        </span>
    );
};

export default Status;
