import { Button, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { downloadFile } from '../../../utils';
import csvjson from 'csvjson';
import React from 'react';

const useStyles = makeStyles(theme => ({
    chartHeaderRoot: {
        marginBottom: '10px',
        display: 'flex',
    },
    chartTitle: {
        marginTop: '5px',
        fontSize: '16px',
        flex: 1,
    },
    exportBtn: {
        padding: '5px 10px',
        textTransform: 'none',
        color: '#3994F0',
    },
    exportIcon: {
        paddingRight: '5px',
    },
}));

export default React.memo(({ title, data }) => {
    const { chartHeaderRoot, chartTitle, exportBtn, exportIcon } = useStyles();

    const handleExport = () => {
        const file = csvjson.toCSV(data, { delimiter: ',', headers: 'key' });
        downloadFile(file, `${title}.csv`);
    };

    return (
        <div className={chartHeaderRoot}>
            <Typography className={chartTitle}>{title}</Typography>
            <Button className={exportBtn} variant='contained' onClick={handleExport}>
                <img src='/export.svg' alt='export' className={exportIcon} />
                Export CSV
            </Button>
        </div>
    );
});
