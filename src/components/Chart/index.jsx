import React, { useEffect, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';
import ChartHeader from './ChartHeader';
import PropTypes from 'prop-types';
import ChartJS from 'chart.js';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: '20px',
    },
}));

const defaultColors = (tr = 1) => [
    `rgba(255, 99, 132, ${tr})`,
    `rgba(54, 162, 235, ${tr})`,
    `rgba(255, 206, 86, ${tr})`,
    `rgba(75, 192, 192, ${tr})`,
    `rgba(153, 102, 255, ${tr})`,
    `rgba(255, 159, 64, ${tr})`,
];

const Chart = React.memo(
    ({
        chartId,
        options = {},
        itemsList = [],
        itemsKeys,
        data,
        type = 'bar',
        useDefaultColors,
        xLabels,
        simpleData,
        datasetProps,
        title,
    }) => {
        const id = chartId || Math.random().toString(36).substr(2, 9);
        const classes = useStyles();

        const buildData = useCallback(() => {
            const clrs = useDefaultColors
                ? {
                      backgroundColor: defaultColors(0.2),
                      borderColor: defaultColors(),
                  }
                : {};

            let datasets;

            if (simpleData) {
                datasets = [{ data: simpleData, ...clrs, ...datasetProps }];
            } else {
                datasets = Object.keys(itemsKeys || itemsList[0] || {}).map(key => ({
                    label: itemsKeys ? itemsKeys[key] : key,
                    data: itemsList.map(itm => itm[key]),
                    ...clrs,
                    ...datasetProps,
                }));
            }

            return {
                labels: (simpleData || itemsList).map((_, ind) => (xLabels ? xLabels[ind] || ind : ind)),
                datasets,
            };
        }, [datasetProps, itemsKeys, itemsList, simpleData, useDefaultColors, xLabels]);

        useEffect(() => {
            new ChartJS(document.getElementById(id), { type, data: data || buildData(), options });
        }, [buildData, itemsList, data, options, id, type]);

        return (
            <Paper className={classes.paper}>
                <ChartHeader title={title} data={simpleData} />
                <canvas id={id} />
            </Paper>
        );
    },
);

Chart.propTypes = {
    useDefaultColors: PropTypes.bool,
    borderWidth: PropTypes.number,
    itemsList: PropTypes.array,
    options: PropTypes.object,
    chartId: PropTypes.string,
    xLabels: PropTypes.array,
    type: PropTypes.string,
    data: PropTypes.object,
};

Chart.defaultProps = {
    datasetProps: { borderWidth: 1, fill: false },
    options: { legend: false },
};

export default Chart;
