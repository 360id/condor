import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { makeStyles } from '@material-ui/core/styles';
import { Container } from '@material-ui/core';
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CustomModal from '../Modal';
import { Modal } from '../';

const useStyles = makeStyles(() => ({
    container: {
        width: '400px',
    },
}));

const TEST_PRICE = 50;

const StripeModal = React.memo(({ onOk, rowToEdit, ...rest }) => {
    // const { organization } = useSelector(store => store.organization)
    const [isLoading, $isLoading] = useState(false);
    const elements = useElements();
    const classes = useStyles();
    const stripe = useStripe();

    const handleOk = async () => {
        if (!stripe || !elements) {
            console.error(
                'Stripe has not loaded yet. Make sure to disable form submission until Stripe.js has loaded.',
            );
            return;
        }
        $isLoading(true);
        const { error, paymentMethod } = await stripe.createPaymentMethod({
            // billing_details: { organizationId: organization.id },
            card: elements.getElement(CardElement),
            type: 'card',
        });

        $isLoading(false);
        if (error) {
            console.error('[error]', error);
        } else {
            console.log('[PaymentMethod]', paymentMethod);
            // console.log({ rowToEdit })
            onOk();
        }
    };
    const options = {
        style: {
            base: {
                '::placeholder': { color: '#aab7c4' },
                fontSize: '16px',
                color: '#424770',
            },
            invalid: {
                color: '#9e2146',
            },
        },
    };
    return (
        <Modal title='Payment data' onOk={handleOk} onOkLabel={`Pay ${TEST_PRICE}$`} onOkLoading={isLoading} {...rest}>
            <Container className={classes.container}>
                <CardElement options={options} />
            </Container>
        </Modal>
    );
});

CustomModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onOk: PropTypes.func,
};

export default StripeModal;
