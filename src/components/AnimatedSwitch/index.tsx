import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { Route, Switch, useLocation } from 'react-router-dom';
import { Error404 } from '..';
import React from 'react';

type Routes = {
    exact: boolean;
    path: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    component: any;
};
interface AnimatedSwitchProps {
    children?: React.ReactChild;
    routes: Routes[];
    isNotFoundRoute: boolean;
}

const AnimatedSwitch: React.FC<AnimatedSwitchProps> = ({ children, routes, isNotFoundRoute }) => {
    const location = useLocation();

    return (
        <TransitionGroup className='css-transition-container'>
            <CSSTransition key={location.key} classNames='fade' timeout={{ enter: 300, exit: 300 }}>
                <section className='css-transition-object'>
                    <Switch location={location}>
                        {routes.map(itm => (
                            <Route exact={itm.exact} path={itm.path} component={itm.component} key={itm.path} />
                        ))}
                        {isNotFoundRoute ? <Route component={Error404} /> : null}
                        {children}
                    </Switch>
                </section>
            </CSSTransition>
        </TransitionGroup>
    );
};

export default AnimatedSwitch;
